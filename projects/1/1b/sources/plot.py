import numpy as np

import matplotlib.pyplot as plt

# Generate x-axis data
block_sizes = [2**i for i in range(7)]

# Generate y-axis data
average_performance = [2.19701,3.77668,5.48538,6.3875,6.13421,5.41058,5.8973]

# Create the plot
plt.plot(block_sizes, average_performance)

# Set the x-axis label
plt.xlabel('Block-Size', fontsize=14)

# Set the y-axis label
plt.ylabel('Average % of Peak Performance', fontsize=14)

plt.xscale('log', base=2)
plt.xticks(block_sizes, ['2^{}'.format(i) for i in range(7)])
# Show the plot
plt.savefig('blockSizesVsPeakPerformance.png')