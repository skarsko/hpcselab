\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\input{assignment.sty}

%Custom Code
\usepackage{listings}
\usepackage{sourcecodepro}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{amsmath}
\usepackage{float}
\usepackage{makecell}

\graphicspath{{images/}} 

\lstset{basicstyle=\footnotesize\ttfamily, language=bash, showstringspaces=false}
\lstset{frame=single}

\newcommand{\br}{\vspace{8pt}\\}
\newcommand{\brr}{\vspace{8pt}}
%End Custom Code

\begin{document}


\setassignment
\setduedate{11 March 2024, 23:59}

\serieheader{High-Performance Computing Lab for CSE}{2024}
            {Student: Samuel Karsko}
            {Discussed with: Alex Pietak (apietak)}{Solution for Project 1a \& 1b}{}

\section{Euler warm-up}
	1.) With a module system, one can load preloaded binaries (e.g. applications or compilers) such that one can use it. \br
	2.) Slurm is not an acronym\footnote{\url{https://slurm.schedmd.com/faq.html}}, Slurm a scheduler, that manages submitted tasks by possibly many users at the same time. Scheduling usually involves telling Slurm the needed ressources and eventually also additional informations (e.g. projected wall-runtime), such that Slurm can efficiently assign the available hardware to the tasks. \br
	3.) We can use the \lstinline|gethostname| function from the header \lstinline|unistd| to obtain a C++-program, which prints the hostname on the machine it's running on. \br
	4.) Running my script with \lstinline|sbatch| and my launch script results in:
	\begin{lstlisting}
[skarsko@eu-login-21 source]$ cat printHostName-48565976.out 
Model name:            AMD EPYC 9654 96-Core Processor
Hello from: eu-g9-046-1		
	\end{lstlisting}  \vspace{8pt}
	5.) Running the script for 2 nodes results in two tasks being launched, on two different nodes:
	\begin{lstlisting}
[skarsko@eu-login-42 source]$ cat printHostName-48568940.out 
Model name:            AMD EPYC 7763 64-Core Processor
Hello from: eu-a2p-380
Hello from: eu-a2p-381
	\end{lstlisting}
		
\section{Performance characteristics}

\subsection{Peak performance}
We find in the Euler's documentation\footnote{\url{https://scicomp.ethz.ch/wiki/Euler}} that Euler VII consists of two phases, with unique AMD EPYC processors, as seen in Figure \ref{fig:eulerVII}.
\begin{figure}[H]
	\centering
	\includegraphics[width=380px]{eulerVII_architecture}
	\caption{Architecture of Euler VII}
	\label{fig:eulerVII}
\end{figure}
The older EPYC 7H12 CPU's belongs to the Rome ZEN 2 architecture, whereas the newer EPYC 7763 belongs to the Milan ZEN 3 architecture. From \footnote{\url{https://en.wikichip.org}} we find, that both architectures do indeed support 3-Operand Fused-Multiply-Add and therefor $n_\text{FMA}=2$. Also, we find that both processors support AVX2 (but not AVX-512) which has a vector register width of 256 bits. This means, we can store 4 double-precision floating point numbers (64 bits) in one register, giving us $n_\text{SIMD}=4$.
Using \footnote{\label{uops}\url{https://uops.info}} we find, that both processors can perform FMA instructions on two execution ports 0 and 1 in parallel, yielding $n_\text{super}=2$. All these specifications can be found in Table \ref{tab:amd_epyc_specs}.
\begin{table}[H]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline 
		& \textbf{AMD EPYC 7H12} & \textbf{AMD EPYC 7763} \\\hline
		$f_\text{nom.}$ & 2.6 GHz & 2.45 GHz \\\hline
		$n_\text{SIMD}$ & 4 & 4 \\\hline
		$n_\text{FMA}$ & 2 & 2 \\\hline 
		$n_\text{SUPER}$ & 2 & 2 \\\hline
	\end{tabular}
	\caption{Comparison of AMD EPYC Processor Specifications}
	\label{tab:amd_epyc_specs}
\end{table}
We can now directly calculate
\begin{align*}
	&P_\text{core,7H12} = 2 * 2 * 4 * 2.6\text{ GHz} = 41.6 \text{ GFlops/s} \\
	&P_\text{core,7763} = 2 * 2 * 4 * 2.45\text{ GHz} = 39.2 \text{ GFlops/s} \\
\end{align*}
Both CPU's have 64 physical cores, therefor $n_\text{cores}=64$, resulting in
\begin{align*}
	&P_\text{CPU,7H12} = 64 * 41.6 \text{ GFlops/s} = 2662.4 \text{ GFlops/s} \\
	&P_\text{CPU,7763} = 64 * 39.2 \text{ GFlops/s} = 2508.8 \text{ GFlops/s} \\
\end{align*}
Euler VII phase 1 and Euler VII phase 2 have 2 CPU's per node each and therefor $n_\text{sockets} = 2$, resulting in
\begin{align*}
	&P_\text{node,7H12} = 2 * 2662.4 \text{ GFlops/s} = 5324.8 \text{ GFlops/s} \\
	&P_\text{node,7763} = 2 * 2508.8 \text{ GFlops/s} = 5017.6 \text{ GFlops/s} \\
\end{align*}
Euler VII phase 1 has 292 nodes, i.e. $n_\text{nodes}=292$ and phase 2 has 284 nodes, i.e. $n_\text{nodes}=248$. Finally we can compute the theoretical peak Flops/s for both clusters: 
\begin{align*}
	&P_\text{cluster,7H12} = 292 * 5324.8 \text{ GFlops/s} = 1'554'841.6 \text{ GFlops/s} \approx 1.555 \text{PFlops/s} \\
	&P_\text{cluster,7763} = 284 * 5017.6 \text{ GFlops/s} = 1'424'998.4 \text{ GFlops/s} \approx 1.425 \text{PFlops/s} \\	
\end{align*}

\subsection{Memory Hierarchies}
To target the Euler VII phase 1 and phase 2 nodes, we'll use the \lstinline|--constraint| flag in the slurm script. This results in following script, which gets the desired information from the processors present in the Euler VII phase 1 node:
\begin{lstlisting}[language=bash]
#!/bin/bash
#SBATCH --job-name=eulerVIIphase1Analysis  # Job name    
#SBATCH --output=eulerVIIphase1-%j.out 	   # Output file 
#SBATCH --error=eulerVIIphase1-%j.err 	   # Error file  
#SBATCH --ntasks=1 	             	   # Number of tasks
#SBATCH --nodes=1                    	   # Number of nodes
#SBATCH --cpus-per-task=1           	   # Number of CPUs per task
#SBATCH --mem-per-cpu=1024           	   # Memory per CPU
#SBATCH --time=00:00:10             	   # Wall clock time limit
#SBATCH --constraint=EPYC_7H12       	   # constrain to EPYC-7H12 CPU model

printf ".......... \n lscpu \n \n"
lscpu 
printf ".......... \n \n"

printf ".......... \n cat /proc/meminfo | head -n 3 \n \n"
cat /proc/meminfo | head -n 3
printf ".......... \n \n"

printf ".......... \n hwloc-ls --whole-system --no-io -f --of fig EPYC-7H12.fig \n \n"
hwloc-ls --whole-system --no-io -f --of fig EPYC-7H12.fig
printf ".......... \n \n"
\end{lstlisting}
A very similar script can be created to target the EPYC-7663 processors of the Euler VII phase 2 nodes. Using the \lstinline|fig2dev| command, provided by the Xfig\footnote{\url{https://www.xfig.org/}} package, we can create two PDFs, which show the memory hierarchy for both processors. The command returns a memory hierarchy figure of all cores per node, and therefor represents two CPUs. All PDFs and outputs of the other commands are provided in the source-files. 


\subsubsection{Cache and main memory size}
We find, that both processors have private L1i, L1d (both 32KB) and L2 Caches (512KB). However, the EPYC 7H12 shares an L3 cache (16MB) among 4 cores, whereas the EPYC 7763 shares it's L3 cache of double the size (32MB) shared among 8 cores. The lower number of cores per L3 cache might help the 7H12 with latency, as the caches and the data stored in them is physically closer to the core. The 7763 however might be more suitable for applications, where larger cache sizes are beneficial and where a lot of cores might for example access one gigantic matrix, spanning the entire 32MB. Both processors have a total of 64 cores and therefor both have a total L3 cache size of 256MB. 
Also, both processors don't have a L4 cache.

\subsection{Bandwidth: STREAM benchmark}
We can download the \lstinline|stream.c| and \lstinline|mysecond.c| files using \lstinline|wget| on the Euler cluster. We know, that both phase 1 and phase 2 Euler VII nodes have CPUs with 256MB L3 caches. This means, that the STREAM-array must have a size of at least 256MB*4 = 1024MB = 1'024'000'000 bytes. As one vector element of type double has a size of 8 bytes, \lstinline|STREAM_ARRAY_SIZE| must be set to at least 1'024'000'000 / 8 = 128'000'000 elements. 
This gives an upper array size of 128'000'000*8 = 1 GB and therefor a total memory requirement of around 3GB. To play it safe, we assign 4GB via slurm. We can compile STREAM using gcc with the appropriate preprocessor variables:
\begin{lstlisting}
gcc -O3 -march=native -DSTREAM_TYPE=double \
	-DSTREAM_ARRAY_SIZE=128000000 -DNTIMES=20 stream.c -o run.exe
\end{lstlisting}
Using the \lstinline|targetPhase1.sh| and \lstinline|targetPhase2.sh| we can run the STREAM benchmark on CPUs from both phases of the Euler VII cluster. We can see the results of the STREAM	benchmarks for both processors in the tables \ref{stream_7H12} and \ref{stream_7663}.
\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		\textbf{Function}    &Best Rate MB/s & Avg time  &   Min time  &   Max time \\\hline
		Copy:       &    31875.0  &   0.077532   &  0.064251   &  0.121724 \\\hline
		Scale:      &    18944.7  &   0.116911   &  0.108104   &  0.138909 \\\hline
		Add:        &    20574.2  &   0.170446   &  0.149313   &  0.207677 \\\hline
		Triad:      &    20857.1  &   0.171024   &  0.147288   &  0.236532 \\\hline		
	\end{tabular}
	\caption{STREAM benchmark on AMD EPYC 7H12}
	\label{stream_7H12}
\end{table}
\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		\textbf{Function}    &Best Rate MB/s & Avg time  &   Min time  &   Max time \\\hline
		Copy:       &    36509.6   &  0.067668   &  0.056095   &   0.106759 \\\hline
		Scale:      &    22348.6   &  0.112311   &  0.091639   &   0.186611 \\\hline
		Add:        &    24913.8   &  0.147154   &  0.123305   &   0.180004 \\\hline
		Triad:      &    23826.1   &  0.149222   &  0.128934   &   0.177603 \\\hline
	\end{tabular}
	\caption{STREAM benchmark on AMD EPYC 7663}
	\label{stream_7663}
\end{table}
Ignoring the bandwidth resulting from copying, we get the rough estimate for the bandwidth
\begin{align*}
	b_\text{STREAM,7H12} \approx 20\text{GB/s} \\
	b_\text{STREAM,7763} \approx 23\text{GB/s}
\end{align*}
so roughly an 15\% increase for the newer EPYC 7763 CPU. 
\br \emph{Note: The STREAM files have not been altered, but can be still found in the source directory.}
\subsection{Performance model: A simple roofline model}
The naive roofline model is the curve
\begin{align*}
	P_\text{max} = \min\left(P_\text{peak}, I \cdot b_\text{max}\right)
\end{align*}
where $P_\text{max}$ is the upper bound for the machines performance, $P_\text{peak}$, the peak performance of operations per second and $b_\text{max}$, the peak bandwith of bytes per second. $I$ is refered to as \emph{Operational Intensity} (Flops per Bytes) and is the $x$-Axis in the naive roofline model.\footnote{S.Williams, A.Waterman, D.Patterson \emph{Roofline:An insightful Visual}\\\url{https://zenodo.org/records/1236156}}
Using my script \lstinline|plot.py| we can plot this curve to get the rooftop-model for both the EPYC 7H12 and EPYC 7663, as seen in Figure \ref{fig:roofline_model}.
\begin{figure}[H]
	\centering
	\includegraphics[width=330px]{roofline}
	\caption{Naive Roofline-Model for AMD EPYC 7H12 and AMD EPYC 7663}
	\label{fig:roofline_model}
\end{figure}
We can compute the operational intensity corresponding to the ridge between compute- and memory-bound using the formula from before:
\begin{align*}
	P_\text{peak} = I \cdot b_\text{max} \ \Rightarrow \begin{cases}
		I_\text{7H12} = \frac{1.555 \cdot 10^{15} \text{Flops}}{2.0 * 10^{10} \text{Bytes/sec.}} \approx 77750 \text{Flops/Byte} \\
		I_\text{7663} = \frac{1.425 \cdot 10^{15} \text{Flops}}{2.3 * 10^{10} \text{Bytes/sec.}} \approx 61957 \text{Flops/Byte}
	\end{cases}
\end{align*}
This also aligns visually with the ridges seen in Figure \ref{fig:roofline_model}.
We can now deduce, that for $I < I_\text{7H12}$ or $I < I_\text{7663}$, an application is memory-bound and for $I > I_\text{7H12}$ or $I > I_\text{7663}$, an application is compute-bound for the AMD EPYC 7H12 and EPYC 7663 respectively.


\section{Auto-vectorization}
1.) Aligned data structures are important for fast memory access, which results in better performance. Misaligned data structures are not aligned with the optimal memory acess patterns, for which the hardware is optimized. Therefor, the CPUs might need extra time finding and accessing data in misaligned structures. \br
2.) There are a lot of obstacles which can hinder automatic compiler vectorization. Generally, the "simpler" the loop, the better. Complex \lstinline|if|-branches or branch-like expressions (\lstinline|return| or \lstinline|goto|) can prevent the compiler from vectorizing the loop. Generally, if it can't be prooven syntactically that vectorizing the loop will have the exact same outcome as not vectorizing the loop , the compiler won't vectorize it. Also, the use of direct memory access trough pointers should be done carfeully; the compiler often can't tell if it's safe to vectorizie the loop and therefor will not vectorize it. The use of classical arrays is therefor advised. \br
3.) The most well-known technique, which is used to aid the compiler in automatic vectorization is the use of \emph{Pragmas}. Pragmas are compiler directives which tell the compiler additional information about the code, e.g. if there are any data dependencies, how big the typical trip cout of the loop is. Pragmas can also straightforward tell the compiler, if the loop should be vectorized or not. Pragmas can be initiated with the \lstinline|#pragma| directive followed by the instruction, for example:
\begin{lstlisting}
//The data is independent
#pragma ivdep

//Vectorize the loop
#pragma vector always

//Assert, that the data is aligned 
#pragma vector align
\end{lstlisting}
\brr
4.) One major vectorizing technique used by the compiler is \emph{Strip-Mining}, where a large loop is stripped down into smaller segments. This increases the cache locality of the single strips and enables to use one single SIMD operation for 4 floating-point data items (at least with Intel SSE). A further optimization technique for matrix-multiplications is \emph{loop interchanging}. Loop interchanging in the context of matrix-multiplication is the reordering of the two innermost loops (compared to "mathematical" matrix-matrix-multiplication notation). This doesn't change the calculations, but enables better iteration of data close to each other. Basically we would compute access
\begin{lstlisting}
for (int k = 0; k < N; k++)
	for(int j = 0; j < N; k++)
		c[i][j] = b[k][j] + ...
\end{lstlisting}
instead of
\begin{lstlisting}
for (int j = 0; k < N; k++)
	for(int k = 0; j < N; k++)
		c[i][j] = b[k][j] + ...
\end{lstlisting}
This obviously helps with local access of \lstinline|b[k][j]|, as we now iterate
\begin{lstlisting}
b[k][0], b[k][1], b[k],[2], ...
\end{lstlisting}
instead of
\begin{lstlisting}
b[0][j], b[1][j], b[2][j], ...
\end{lstlisting} \brr
5.) The strategy to help the compiler with vectorization depend on the reason, why the compiler struggles. One often recurring problem arises, if the data is organized as an array of structures. Therefor, if possible, one should implement the code as a structure of arrays, which can help with automatic vectorization. Also, the loops should be arranged in an row-major format. Smaller data types can also help with vectorization, as possibly more data can fit inside one SIMD operation. One can also directly affect the compilers decisions by used pragmas like \lstinline|#pragma vector always| or \lstinline|#pragma ivdep|, as discussed previously.
\section{Matrix multiplication optimization}
After familiarising with the provided templates and submit-scripts we can perform the first run and observe the difference between the naive and BLAS implementation. Whereas the naive implementation didn't even reach 0.5Gflop/s, the BLAS reference implementation reached a total of 50.53Gflop/s for the biggest matrix size (1200*1200). For the attempt of block matrix-matrix-multiplication we should take the size of the \emph{fast memory} on the AMD EPYC 7763 into consideration. From the previous research in this project we know, that said processor has an L3 cache, sized at 256MB. The first attempt at block-optimization will be to ensure, that the single blocks fit into the L3 cache of the CPU. Assuming 8 bytes for a double precision float we find, that $256'000'000 / 8 = 32'000$ doubles can fit into the L3 Cache. As we have 3 matrices, we can compute the theoretical optimal length/width of a matrix block with
\begin{align*}
	n = \sqrt{\frac{32000}{3}} \approx 103
\end{align*}
In the first block-approach, we will use a block-matrix-size of $n=64$, which doesn't cover the whole cache, but should still make a big improvement compared to the naive implementation. With $n=64$, the fast cache will need to hold $12'288$ doubles, so roughly 40\% of the L3 cache. This leads to following implementation:
\begin{lstlisting}
const int blockSize = 64;
for (int kk = 0; kk < n; kk += blockSize) 
	for (int jj = 0; jj < n; jj += blockSize) 
		//Load blocks into fast memory
		for (int i = 0; i < n; i++) 
			for (int j = jj; j < min(jj + blockSize, n); j++) {
				double cij = C[i + j * n];
				for (int k = kk; k < min(kk + blockSize, n); k++) {
					cij += A[i + k * n] * B[k + j * n];
				}
				C[i + j * n] = cij;
			}
\end{lstlisting}
With this implementation we iterate over the blocks with \lstinline|kk| and \lstinline|jj|. \lstinline|i| and \lstinline|j| are then iterating inside a single block an use the minimum function for matrices with sizes which aren't divisible by the block-size. $C_{ij}$ is then stored as a \lstinline|double| variable such that it doesn't need to be loaded from the array multiple times in the innermost loop. This implementation achieved only a vanishingly small improvement in Flops:
\begin{table}[H]
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline 
		\textbf{Metric} & \textbf{Naive} & \textbf{Simple Blocking} & \textbf{OpenBLAS} \\\hline
		Average \% of peak performance & 1.074\% & 1.300\% & 101.815\% \\\hline
		Peak GFlop/s & 0.45GFlop/s & 0.53GFlop/s & 46.63GFlop/s \\\hline
	\end{tabular}
	\caption{Performance of matrix-matrix multiplication with simple blocking with block-size of 64.}
	\label{tab:mm_simple_bock}
\end{table}
But there's one magical button we can press to increase the performance massively: Compiler flags! First we'll just add the highest level of optimisation (\lstinline|-O3|). This yields following results:
\begin{table}[H]
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline 
		\textbf{Metric} & \textbf{Naive} & \textbf{Simple Blocking} & \textbf{OpenBLAS} \\\hline
		Average \% of peak performance & 4.888\% & 7.389\% & 101.366\% \\\hline
		Peak GFlop/s & 2.43GFlop/s & 4.03GFlop/s & 46.36GFlop/s \\\hline
	\end{tabular} 
	\caption{Performance of matrix-matrix multiplication with simple blocking with block-size of 64 and \lstinline|-O3| flag.}
	\label{tab:mm_simple_bock_o3}
\end{table}
We observe the miracles the compiler managed to achieve, increasing the performance by around 500\%. The already heavily optimised BLAS reference implementation however didn't show any performance gains anymore. Now we can explore the effect of using the Intel compiler and compare the difference. Using the Intel compiler with \lstinline|-O3|, \lstinline|-march=core-avx2| and \lstinline|-funroll-loops| flags results in a performance dip for the simple blocking matrix-matrix multiplication back to the average percentage of peak performance of 5.648, roughly 30\% less GFlop/s then the same code compiled with \lstinline|gcc|. However we can use the auto-vectorization techniques discussed previously. I've added two pragmas in the before the innermost loop to help the compiler to vectorize those:
\begin{lstlisting}
double cij = C[i + j * n];
#pragma ivdep
#pragma vector always
for (int k = kk; k < min(kk + blockSize, n); k++){ 
	cij += A[i + k * n] * B[k + j * n];
}	
C[i + j * n] = cij;
\end{lstlisting}
After seeing only very marginal improvements for the original block-size of 64, I experimented with smaller block-sizes, which indeed yielded improvements, documented in figure \ref{fig:blockSizeVsPeakPerformance}.

\begin{figure}[H]
	\centering
	\begin{minipage}{0.4\textwidth}
		\centering
		\begin{tabular}{|c|c|}
			\hline 
			\textbf{Block-Size:} & \textbf{\makecell{Average peak \\ performance}} \\\hline
			64 & 5.898\% \\\hline
			32 & 5.411\% \\\hline
			16 & 6.134\% \\\hline
			8 & 6.388\% \\\hline
			4 & 5.485\% \\\hline
			2 & 3.777\% \\\hline
			1 & 2.197\% \\\hline
		\end{tabular} 
	\end{minipage}%
	\begin{minipage}{0.4\textwidth}
		\centering
		\includegraphics[width=220px]{blockSizesVsPeakPerformance}
	\end{minipage}
	\caption{Effect of Block Size on Percentage of Peak Performance}
	\label{fig:blockSizeVsPeakPerformance}
\end{figure}
From these runs we see, that the optimal blocking size is 8 with a average peak performance of 6.388\%. Interestingly, a block size of 32 marks a dip into the otherwise concave function of block-size vs. average percentage of peak performance. The use of the Intel-Compiler however doesn't reach the peak performance of using \lstinline|gcc| with a block-size of 64 (6.388\% vs 7.389\%).
One factor I've overseen until now is the external minimum-function I'm using. Because it's called as second \lstinline|for|-loop argument it could slow down efficient vectorization, and also, a function has to be called multiple times instead of only once. I quickly fixed this and also added automatic loop-unrolling:
\begin{lstlisting}
int jLim = min(jj + blockSize, n);
for (int j = jj; j < jLim; j++) {
	double cij = C[i + j * n];
	int kLim = min(kk + blockSize, n);
	#pragma ivdep
	#pragma vector always
	#pragma unroll(8)
	for (int k = kk; k < kLim; k++) {
		cij += A[i + k * n] * B[k + j * n];
	}
	C[i + j * n] = cij;
}
\end{lstlisting}
Finally I could improve and reached with automatic loop-unrolling with a factor of 8 an average percentage of peak performance of 8.182\%, making the blocking implementation around 2 times faster then the naive approach. Experimenting with the loop-unrolling factor I found best performance with the factor 4 performing on average at 8.336\% of the peak-performance. All in all, although some improvements were made compared to the naive implementation, my blocked matrix-matrix-multiplication is still far behind the OpenBLAS implementation. The final plot \ref{fig:timing}, comparing the three implementations is therefore quite underwhelming:
\begin{figure}[H]
	\centering
	\includegraphics[width=380px]{timing}
	\caption{Comparision between simple, blocking and BLAS implementation of matrix-matrix multiplication}
	\label{fig:timing}
\end{figure}

\end{document}
