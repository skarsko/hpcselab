#!/bin/bash
#SBATCH --job-name=printHostname      # Job name    (default: sbatch)
#SBATCH --output=printHostName-%j.out # Output file (default: slurm-%j.out)
#SBATCH --error=printHostName-%j.err  # Error file  (default: slurm-%j.out)
#SBATCH --ntasks=2                    # Number of tasks
#SBATCH --nodes=2                     # Number of nodes
#SBATCH --cpus-per-task=1             # Number of CPUs per task
#SBATCH --mem-per-cpu=1024            # Memory per CPU
#SBATCH --time=00:00:10               # Wall clock time limit
#SBATCH --constraint=EPYC_9654        # constrain CPU model

# load some modules & list loaded modules
module load gcc
module list

# print CPU model
lscpu | grep "Model name"

# run (srun: run job on cluster with provided resources/allocation)
srun  helloWorld
