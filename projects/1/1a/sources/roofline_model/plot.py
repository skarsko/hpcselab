import matplotlib.pyplot as plt
import numpy as np

savePng = True

# Set up data (*10^15)
PPeak7H12 = 1.555
PPeak7763 = 1.425
bMax7H12 = 2.0e-5
bMax7763 = 2.3e-5

# Set up the axis 
x = np.linspace(0, 2e5)
y7H12 = np.minimum(PPeak7H12, x * bMax7H12)
y7763 = np.minimum(PPeak7763, x * bMax7763)

#Set ticks
plt.yticks([0,1,2])
plt.xticks([0,50000,100000,150000,200000])

# Create the plot
plt.plot(x, y7H12, label='EPYC 7H12')
plt.plot(x, y7763, label='EPYC 7763')

# Add labels and title
plt.xlabel('Operational Intensity [PFlops/Byte]')
plt.ylabel('Performance [PFlops]')
plt.title('Naive Roofline Model')

# Legend & plot
plt.legend()
if savePng:
    plt.savefig('roofline.pdf', format='pdf')
else:
    plt.show()