#!/bin/bash
#SBATCH --job-name=eulerVIIphase1Analysis  # Job name    (default: sbatch)
#SBATCH --output=eulerVIIphase1-%j.out 	   # Output file (default: slurm-%j.out)
#SBATCH --error=eulerVIIphase1-%j.err 	   # Error file  (default: slurm-%j.out)
#SBATCH --ntasks=1 	             	   # Number of tasks
#SBATCH --nodes=1                    	   # Number of nodes
#SBATCH --cpus-per-task=1           	   # Number of CPUs per task
#SBATCH --mem-per-cpu=1024           	   # Memory per CPU
#SBATCH --time=00:00:10             	   # Wall clock time limit
#SBATCH --constraint=EPYC_7H12       	   # constrain to EPYC-7763 CPU model

# print CPU model
printf ".......... \n lscpu \n \n"
lscpu 
printf ".......... \n \n"

printf ".......... \n cat /proc/meminfo | head -n 3 \n \n"
cat /proc/meminfo | head -n 3
printf ".......... \n \n"

printf ".......... \n hwloc-ls --whole-system --no-io -f --of fig EPYC-7763.fig \n \n"
hwloc-ls --whole-system --no-io -f --of fig --restrict numanode:0 AMD_EPYC_7H12.fig
printf ".......... \n \n"

