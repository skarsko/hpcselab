#include <iostream> //for priting
#include <unistd.h> //for gethostname

int main(){
    //Get Hostname of the machine
    char hostname[1024];
    gethostname(hostname, 1024); //Yes, very memory safe.
    std::cout << "Hello from: " << hostname << std::endl;
    return 0;
}