const char* dgemm_desc = "Blocked dgemm.";


/* min function for integeres */
int min(int a, int b) {
  return a < b ? a : b;
}

/* This routine performs a dgemm operation
 *
 *  C := C + A * B
 *
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values.
 */
void square_dgemm(int n, double* A, double* B, double* C) {
    const int blockSize = 8;
    for (int kk = 0; kk < n; kk += blockSize) {
        for (int jj = 0; jj < n; jj += blockSize) {
            // Load blocks into fast memory
            for (int i = 0; i < n; i++) {
              int jLim = min(jj + blockSize, n);
                for (int j = jj; j <jLim; j++) {
                    double cij = C[i + j * n];
                    int kLim = min(kk + blockSize, n);
                    //Pragma directives to vectorize the inner loop
                    #pragma ivdep
                    #pragma vector always
                    #pragma unroll(4)
                    for (int k = kk; k < kLim; k++) {
                        cij += A[i + k * n] * B[k + j * n];
                    }
                    C[i + j * n] = cij;
                }
            }
        }
    }
}

