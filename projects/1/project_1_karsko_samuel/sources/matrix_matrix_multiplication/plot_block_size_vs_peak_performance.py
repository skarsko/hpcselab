#Simple plotting script to generate the plot "Effect of Block Size on Percentage of Peak Performance
#Requires Matplotlib (https://matplotlib.org/)
#
#
import matplotlib.pyplot as plt
#
block_sizes = [2**i for i in range(7)]
average_performance = [2.19701,3.77668,5.48538,6.3875,6.13421,5.41058,5.8973]
#
plt.plot(block_sizes, average_performance)
plt.xlabel('Block-Size', fontsize=14)
plt.ylabel('Average % of Peak Performance', fontsize=14)
#
plt.xscale('log', base=2)
plt.xticks(block_sizes, ['2^{}'.format(i) for i in range(7)])
#
plt.show()
plt.savefig('blockSizesVsPeakPerformance.png')
