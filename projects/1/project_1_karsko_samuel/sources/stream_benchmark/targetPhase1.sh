#!/bin/bash
#SBATCH --job-name=streamEulerVIIphase1    # Job name    
#SBATCH --output=streamPhase1-%j.out 	   # Output file 
#SBATCH --error=streamPhase1-%j.err 	   # Error file  
#SBATCH --ntasks=1 	             	   # Number of tasks
#SBATCH --nodes=1                    	   # Number of nodes
#SBATCH --cpus-per-task=1           	   # Number of CPUs per task
#SBATCH --mem-per-cpu=2048           	   # Memory per CPU
#SBATCH --time=00:02:00             	   # Wall clock time limit
#SBATCH --constraint=EPYC_7H12       	   # constrain to EPYC-7H12 CPU model

./run.exe
