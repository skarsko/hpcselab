#!/bin/bash
#SBATCH --job-name=streamEulerVIIphase2    # Job name    
#SBATCH --output=streamPhase2-%j.out 	   # Output file 
#SBATCH --error=streamPhase2-%j.err 	   # Error file  
#SBATCH --ntasks=1 	             	   # Number of tasks
#SBATCH --nodes=1                    	   # Number of nodes
#SBATCH --cpus-per-task=1           	   # Number of CPUs per task
#SBATCH --mem-per-cpu=2048           	   # Memory per CPU
#SBATCH --time=00:02:00             	   # Wall clock time limit
#SBATCH --constraint=EPYC_7763       	   # constrain to EPYC-7H12 CPU model

./run.exe
