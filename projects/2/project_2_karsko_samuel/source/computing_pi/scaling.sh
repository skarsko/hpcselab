#!/bin/bash
#SBATCH --job-name=weakScaling      # Job name    (default: sbatch)
#SBATCH --output=out/scaling-%j.out # Output file (default: slurm-%j.out)
#SBATCH --error=out/scaling-%j.err  # Error file  (default: slurm-%j.out)
#SBATCH --ntasks=1                    # Number of tasks
#SBATCH --cpus-per-task=2             # Number of CPUs per task
#SBATCH --mem-per-cpu=1024            # Memory per CPU
#SBATCH --time=00:01:30               # Wall clock time limit
#SBATCH --constraint=EPYC_7763        # constrain CPU model

# load some modules & list loaded modules
module load gcc
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
iters=1024000000
echo "Running on " $iters " iterations with " $SLURM_CPUS_PER_TASK " threads."
echo
echo "Serial: " 
./pi_serial $iters
echo "OpenMP Critical: "
./pi_omp_critical $iters
echo "OpenMP Reduction: "
./pi_omp_reduction $iters
echo
echo
echo