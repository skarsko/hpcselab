#!/bin/bash

ml load gcc
[ -f output.txt ] && rm output.txt

# Define the range and step for RECURSION_LENGTH
start=5000
end=10000000
step=5000
FIXED_RECURSION_LENGTH=100000
FIXED_LIST_SIZE=1000000
RECURSION_LENGTH=0
LIST_SIZE=0

NUM_OF_RUNS_PER_RECURSION_LENGTH=10

export OMP_NUM_THREADS=32


for (( LIST_SIZE = start; LIST_SIZE <= end; LIST_SIZE += step )); do
    echo "Running with LIST_SIZE=${LIST_SIZE}"
    make clean
    make 
    echo "LIST_SIZE=" ${LIST_SIZE} >> output.txt
    for (( i = 1; i <= NUM_OF_RUNS_PER_RECURSION_LENGTH; i++ )); do
        ./quicksort $LIST_SIZE $FIXED_RECURSION_LENGTH >> output.txt
    done
    echo "----------------------------------------" >> output.txt
done

#
#for (( RECURSION_LENGTH = start; RECURSION_LENGTH <= end; RECURSION_LENGTH += step )); do
#    echo "Running with RECURSION_LENGTH=${RECURSION_LENGTH}"
#    make clean
#    make 
#    echo "RECURSION_LIMIT=" ${RECURSION_LENGTH} >> output.txt
#    for (( i = 1; i <= NUM_OF_RUNS_PER_RECURSION_LENGTH; i++ )); do
#        ./quicksort $FIXED_LIST_SIZE $RECURSION_LENGTH >> output.txt
#    done
#    echo "----------------------------------------" >> output.txt
#done

ml load python
python computeMedian.py
python plotRecursionLimit.py