#!/bin/bash
#SBATCH --job-name=quickSort
#SBATCH --output=quickSort.out
#SBATCH --error=quickSort.err
#SBATCH --nodes=1
#SBATCH --cpus-per-task=32

export OMP_NUM_THREADS=32

srun -n 1 --cpus-per-task=32 launch.sh