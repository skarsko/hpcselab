def calculate_average_times(output_file):
    with open(output_file, 'r') as file:
        lines = file.readlines()

    recursion_limit = None
    times = []
    averages = []

    for line in lines:
        if "RECURSION_LIMIT=" in line:
            if recursion_limit is not None and times:
                average_time = sum(times) / len(times)
                averages.append((recursion_limit, average_time))
                times = []
            recursion_limit = int(line.split('=')[1].strip())
        elif "elapsed time[s]" in line:
            # Try to find the first floating point number in the line
            time_str = next((word for word in line.split() if '.' in word), None)
            if time_str:
                try:
                    time = float(time_str)
                    times.append(time)
                except ValueError:
                    print(f"Warning: Could not convert '{time_str}' to float. Skipping line: {line.strip()}")

    if recursion_limit is not None and times:
        average_time = sum(times) / len(times)
        averages.append((recursion_limit, average_time))

    return averages


def write_averages_to_file(averages, output_file="averages.txt"):
    with open(output_file, 'w') as file:
        for recursion_limit, avg_time in averages:
            file.write(f"{recursion_limit},{avg_time:.6e}\n")

# Adjust 'output.txt' to the path of your output file
averages = calculate_average_times("output.txt")
write_averages_to_file(averages)