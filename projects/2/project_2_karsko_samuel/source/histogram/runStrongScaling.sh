#!/bin/bash
#SBATCH --job-name=hist_sSc      # Job name
#SBATCH --output=hist_sSc-%j.out # Output file
#SBATCH --error=hist_sSc-%j.err  # Error file
#SBATCH --ntasks=1               # Number of tasks
#SBATCH --constraint=EPYC_7763   # Select node with CPU
#SBATCH --cpus-per-task=48       # Number of CPUs per task
#SBATCH --mem-per-cpu=1024       # Memory per CPU
#SBATCH --time=00:10:00          # Wall clock time limit

# Load some modules & list loaded modules
module load gcc

# Compile
make clean
make

# Run
export OMP_NUM_THREADS=1
echo "Running hist_seq: "
./hist_seq
echo "------------------------"
echo "Running hist_omp with 1 thread: "
./hist_omp
for i in 2, 4, 8, 12, 16, 20, 24, 28, 32, 40, 48
do
    export OMP_NUM_THREADS=$i
    echo "------------------------"
    echo "Running hist_omp with $i threads: "
    ./hist_omp
done
echo "------------------------"
echo "All runs completed"
echo "------------------------"