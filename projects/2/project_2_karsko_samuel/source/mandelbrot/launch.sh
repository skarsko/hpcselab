#!/bin/bash
#SBATCH --job-name=mandelbrot
#SBATCH --output=out/mandelbrot-%j.out
#SBATCH --error=out/mandelbrot-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=4
#SBATCH --time=00:10:00
#SBATCH --constraint=EPYC_7763

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

./mandel_omp
