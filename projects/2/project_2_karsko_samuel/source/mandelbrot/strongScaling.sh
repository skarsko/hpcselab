#!/bin/bash

# Array of thread counts
threads=(1 2 4 8 16 32)

# Loop through the thread counts
for t in "${threads[@]}"
do
    # Adjust the --cpus-per-task parameter
    sed -i "s/#SBATCH --cpus-per-task=.*/#SBATCH --cpus-per-task=$t/" launch.sh

    # Submit the job using sbatch
    echo "Submitting job with $t threads: "
    sbatch launch.sh
    echo "---------------------------------"

done

#Notes
#Submitting job with 1 threads: 
#Submitted batch job 51013154
#---------------------------------
#Submitting job with 2 threads: 
#Submitted batch job 51013155
#---------------------------------
#Submitting job with 4 threads: 
#Submitted batch job 51013158
#---------------------------------
#Submitting job with 8 threads: 
#Submitted batch job 51013166
#---------------------------------
#Submitting job with 16 threads: 
#Submitted batch job 51013171
#---------------------------------
#Submitting job with 32 threads: 
#Submitted batch job 51013175