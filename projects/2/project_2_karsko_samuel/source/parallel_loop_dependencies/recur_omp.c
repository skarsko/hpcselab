#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "walltime.h"
#include <omp.h>

double fast_exp(double base, int power);

int main(int argc, char *argv[]) {
  int N = 2000000000;
  double up = 1.00000001;
  double Sn = 1.00000001;
  int n;

  /* allocate memory for the recursion */
  double *opt = (double *)malloc((N + 1) * sizeof(double));
  if (opt == NULL) {
    perror("failed to allocate problem size");
    exit(EXIT_FAILURE);
  }

  double time_start = walltime();

  int num_threads = omp_get_max_threads();
  double m = 1.00000001;
  int dist = N / num_threads;
  //Compute m = up^dist
  m = fast_exp(up, dist-1);
  opt[0] = 1.00000001;

  //Compute checkpoints (opt[dist], opt[2*dist], opt[3*dist], ..., opt[N-1])
  for(int i = 1; i < num_threads; i++){
    opt[i*dist] = opt[(i-1)*dist] * m;
  }
  //In parallel, compute the rest of the values, starting from the checkpoints
  #pragma omp parallel 
  {
    int thread_id = omp_get_thread_num();
    int start = thread_id * dist;
    int end = (thread_id + 1) * dist;
    if (thread_id == num_threads - 1) {
      end = N; 
    }
    #pragma omp simd
    for (int j = start+1; j < end; j++) {
      opt[j] = opt[j - 1] * up; 
    }
  }

  Sn = opt[N-1];

  printf("Parallel RunTime  :  %f seconds\n", walltime() - time_start);
  printf("Final Result Sn   :  %.17g \n", Sn);

  double temp = 0.0;
  for (n = 0; n <= N; ++n) {
    temp += opt[n] * opt[n];
  }
  printf("Result ||opt||^2_2 :  %f\n", temp / (double)N);
  printf("\n");

  return 0;
}

//Helper function to compute the exponentiation of a number
double fast_exp(double base, int power) {
    double result = 1.0;
    while (power > 0) {
        if (power % 2 == 1) {
            result *= base;  
        }
        base *= base;  
        power /= 2;  
    }
    return result;
}