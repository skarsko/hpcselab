\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\input{assignment.sty}

%Custom Code
\usepackage{listings}
\usepackage{sourcecodepro}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{amsmath}
\usepackage{float}
\usepackage{makecell}

\graphicspath{{images/}} 

\lstset{basicstyle=\footnotesize\ttfamily, language=bash, showstringspaces=false}
\lstset{frame=single}
\lstset{
	showlines=true
}


\newcommand{\br}{\vspace{8pt}\\}
\newcommand{\brr}{\vspace{8pt}}
%End Custom Code

\begin{document}


\setassignment
\setduedate{25 March 2024, 23:59}

\serieheader{High-Performance Computing Lab for CSE}{2024}
            {Student: Samuel Karsko}
            {Discussed with: -}{Solution for Project 2}{}
\newline

\section{Computing $\pi$ with \texttt{OpenMP}}
The reduction OpenMP implementation (Listing \ref{pi_reduction}) of the $\pi$-approximation is straightforward: We can use the OpenMP \lstinline|#pragma omp parallel for reduction(+:sum)| directive right before the \lstinline|for|-loop to parallelize it using OpenMP. For the implementation using the \lstinline|critical| directive (Listing \ref{pi_critical}), we can start a parallel region with \lstinline|#pragma omp parallel| where we store a private variable for the temporary sum for a single \lstinline|OpenMP| thread, called \lstinline|sumP|. Then, the loop is divided in equal parts for every single threads, with the last one iterating until $N$. At the end of the parallel region, we can use the directive \lstinline|#pragma omp critical|, followed with the addition of the temporary to the accumulated sum, i.e. \lstinline|sum += sumP|.
\begin{figure}[H]
	\begin{minipage}{0.48\textwidth}
		\begin{lstlisting}[language=C, caption={Reduction-Implementation}]
#pragma omp parallel for reduction(+:sum)
for (int i = 0; i < N; ++i) {
	double x = (i + 0.5)*h;
	sum += 4.0 / (1.0 + x*x);
}

pi = sum*h;









		\end{lstlisting}
	\label{pi_reduction}
	\end{minipage}\hfill
	\begin{minipage}{0.48\textwidth}
		\begin{lstlisting}[language=C, caption={Critical-Implementation}]
 #pragma omp parallel
{
 double sumP = 0.;
 int numThreads = omp_get_num_threads();
 int threadId = omp_get_thread_num();
 int i = threadId * N / numThreads; 
 int iL = (threadId + 1) * N / numThreads;
 if(threadId == numThreads - 1) iL = N;
 for (; i < iL; ++i) {
  double x = (i + 0.5)*h;
  sumP += 4.0 / (1.0 + x*x);
}
#pragma omp critical
sum += sumP;
}
pi = sum*h;
		\end{lstlisting}
	\label{pi_critical}
	\end{minipage}
\end{figure}
To examine weak scaling we'll vary system size with the number of OMP threads. We choose a linear relationships of $10^6$ iterations per OMP-Thread. For examining strong scaling, we keep the system size fixed at a soid $4\cdot10^6$ iterations with a varying number of OMP threads from 1 to 48. Both the weak- and strong-scaling could be easily launched using the helper-script \lstinline|launchHelper.sh|, which adapts the \lstinline|slurm|-script \lstinline|scaling.sh| and launches all the required runs. The pure walltime-numbers for weak and strong-scaling can be found in tables \ref{pi_weak_scaling} and \ref{pi_strong_scaling}.
	
\begin{table}
		\centering
		\begin{tabular}[H]{|c|c|c|c|c|}
			\hline
			\textbf{Iterations N} & \textbf{Threads} & \textbf{Walltime [s] ser.} & \textbf{Walltime [s] crit.} & \textbf{Walltime [s] red.} \\\hline
			1'000'000 & 1 & 0.00236331 & 0.00265307 & 0.00238664 \\\hline
			2'000'000 & 2 & 0.00454806 & 0.00292116 & 0.00213660 \\\hline
			4'000'000 & 4 & 0.01212243 & 0.00314035 & 0.00234424 \\\hline
			8'000'000 & 8 & 0.01916830 & 0.01102929 & 0.00259702 \\\hline
			16'000'000 & 16 & 0.03871276 & 0.02688431 & 0.00342683 \\\hline
			32'000'000 & 32 & 0.08324717 & 0.02326438 & 0.03294082 \\\hline
			48'000'000 & 48 & 0.10569133 & 0.02015531 & 0.02595424 \\\hline
		\end{tabular}
	\caption{Weak-Scaling of Approximation of $\pi$}
	\label{pi_weak_scaling}
\end{table}		
\begin{table}
		\centering
		\begin{tabular}[H]{|c|c|c|c|c|}
			\hline
			\textbf{Iterations N} & \textbf{Threads} & \textbf{Walltime [s] ser.} & \textbf{Walltime [s] crit.} & \textbf{Walltime [s] red.} \\\hline
			32'000'000 & 1 & 0.05622339 & 0.05613178 & 0.05624762 \\\hline
			32'000'000 & 2 & 0.05647587 & 0.04940684 & 0.06961638 \\\hline
			32'000'000 & 4 & 0.07546324 & 0.01897874 & 0.01897892 \\\hline
			32'000'000 & 8 & 0.07024391 & 0.00990577 & 0.00983166 \\\hline
			32'000'000 & 16 & 0.06815489 & 0.00692436 & 0.01096758 \\\hline
			32'000'000 & 32 & 0.06604297 & 0.02734317 & 0.01128553 \\\hline
			32'000'000 & 48 & 0.08593073 & 0.02460850 & 0.02499132 \\\hline
		\end{tabular}
	\caption{Strong-Scaling of Approximation of $\pi$}
	\label{pi_strong_scaling}
\end{table}			
\begin{figure}[H]
	\centering
	\begin{minipage}[b]{0.495\textwidth}
		\includegraphics[width=\textwidth]{weak_scaling.pdf}
		\caption{Weak Scaling}
		\label{fig:weak_scaling}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{0.495\textwidth}
		\includegraphics[width=\textwidth]{strong_scaling.pdf}
		\caption{Strong Scaling}
		\label{fig:strong_scaling}
	\end{minipage}
\end{figure}
In the weak-scaling graph (Figure \ref{fig:weak_scaling}), wee see, that the serial implementation grows linearly with the number of iterations, as expected. Both OMP implementations scale much better with an increasing problem size. A "perfect" weak scaling graph would be represented by a constant function. Both implementations however are fluctuating and experienced increased wall times for larger problem sizes. The implementation using the reduction generally performs better for smaller problem-sizes, whereas the implementation using the \lstinline|critical|-pragma performs a bit better for larger problem sizes. When we look at the strong-scaling graph (Figure \ref{fig:strong_scaling}) we see, that the serial implementation isn't as constant as it should be. This hints, that there is quite some natural noise causing variability in the runtimes. This is understandable, as the walltimes are very small (typical wall-time for serial implementation ith 32'000'000 iterations is around ~0.07s).However we can clearly see, that both OMP implementations decline on the strong-scaling graph, where the $x$-axis is logarithmic. It seems, that the implementation using the \lstinline|critical|-pragma performs better for a smaller number of thread, and the implementation using the reduction peforms better better for a larger number of threads. This however is a very unreliable observation, as we can estimate the variance from the sequential implementation to be around $\pm 0.01$s, which would make the assumption statistically insignificant. 
This variance could likely be fixed with a higher number of iterations. Therefor I rerun the strong-scaling setup with more runs (at least one run for every extra 4 threads) with a $1024\cdot10^6$ iterations.
\begin{figure}[H]
	\centering
	\includegraphics[width=300px]{strong_scaling_improved.pdf}
	\caption{Strong-Scaling with higher iteration size}
	\label{fig:strong_scaling_improved}
\end{figure}
The results of this improved runplan are captured in Figure \ref{fig:strong_scaling_improved}. Now we see a clearer picture by also plotting the speedup of the serial and OMP implementation. We can observe, that the speedup is of linear nature, peaking at power's of 2 (8,16,32) for the number of OMP threads. It would be expected, that the speedup would stagnate for an larger number of threads, when the overhead of having threads isn't worth the division of workload among them. Also, with this refined graph we can verify, that there is no significant difference between both OMP implementation, if there's enough workload for them. 
\section{The Mandelbrot set  using \texttt{OpenMP}}
We can implement the missing code easily by following the instructions given in the project description. We iterate with a \lstinline|while|-loop until we either reach the maximum number of iterations \lstinline|MAX_ITERS| or reach $|z|>2$. The next $z$ is then computed using
\begin{align*}
	z_{n+1} &= z_n^2 + c = (x + iy)^2 + cx + icy = x^2 + 2ixy - y^2 + cx + icy \\
	&= x^2 -y^2 + cx + i(2xy + cy) 
\end{align*}
and the counter \lstinline|n| is incremented by one. After the \lstinline|while|-loop we increase the total number of iterations by the iterations performed for this pixel, given by the counter \lstinline|n|.
\begin{lstlisting}
while (x2 + y2 <= 4.0 && n < MAX_ITERS) {
	y = 2 * x * y + cy;
	x = x2 - y2 + cx;
	x2 = x * x;
	y2 = y * y;
	n++;
}
nTotalIterationsCount += n;
\end{lstlisting}
After completing the missing code, we're now able to generate a black\&white visualization for the Mandelbrot set, seen in \ref{fig:mandelbrot_set}
\begin{figure}[H]
	\centering
	\includegraphics[width=260px]{mandelbrot_set.png}
	\caption{Black and White Mandelbrot Set with a resolution of 4096$\times$4096}
	\label{fig:mandelbrot_set}
\end{figure}
The sequential solution has a substantial runtime of over 5 minutes on an AMD EPYC 7763 core. The details can be found in Table \ref{table:performance_metrics_sequential_mandelbrot}.
\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		\textbf{Metric}                   & \textbf{Value}          \\ \hline
		Total time                        & 330.229 seconds         \\ \hline
		Image size                        & 4096 x 4096 = 16777216 Pixels \\ \hline
		Total number of iterations        & 113610974266            \\ \hline
		Avg. time per pixel               & 1.96832e-05 seconds     \\ \hline
		Avg. time per iteration           & 2.90667e-09 seconds     \\ \hline
		Iterations/second                 & 3.44036e+08             \\ \hline
		MFlop/s                           & 2752.29                 \\ \hline
	\end{tabular}
	\caption{Performance Metrics of Sequential Mandelbrot Solver on AMD EPYC 7763}
	\label{table:performance_metrics_sequential_mandelbrot}
\end{table}
We can now parallelize this code using a parallel for-loop pragma with a summation reduction clause for the total iteration count placed
before the first \lstinline|for|-loop. This will mean, that multiple threads can work on different columns of the image at the same time. Also we can put all the private variables inside the loop, as they are private for a thread anyway. Also, the incrementation of \lstinline|cy| also needs some adjustment, mainly by computing the increment independently from \emph{how many iterations we've already done}, but rather directly with \lstinline|cy = MIN_Y + j * fDeltaY|. The performance metrics for a parallel run using 8 OMP threads are documented in table \ref{table:performance_metrics_omp_mandelbrot}.
\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		\textbf{Metric}                   & \textbf{Value}          \\ \hline
		Total time                        & 114.956 seconds         \\ \hline
		Image size                        & 4096 x 4096 = 16777216 Pixels \\ \hline
		Total number of iterations        & 113639289737            \\ \hline
		Avg. time per pixel               & 6.85191e-06 seconds     \\ \hline
		Avg. time per iteration           & 1.01159e-09 seconds     \\ \hline
		Iterations/second                 & 9.88546e+08             \\ \hline
		MFlop/s                           & 7908.37                 \\ \hline
	\end{tabular}
	\caption{Performance Metrics of Parallel Mandelbrot Solver on AMD EPYC 7763 with 8 OMP Threads}
	\label{table:performance_metrics_omp_mandelbrot}
\end{table}
To investigate strong scaling we launch 6 jobs with 1,2,4,8,16 and 32 OMP threads. The exact results can be found in table \ref{table:strong_scaling_mandelbrot}. The resulting plot (Figure \ref{fig:strong_scaling_mandelbrot}) shows a nice strong-scaling with an increasing number of OMP threads. Especially using two instead of only one OMP thread results in a speedup of around ~1.9, which is very close to the theoretical maximum speedup of 2. Further increases of OMP threads also still leads to massive speed-ups, although not of the same magnitude. This is understandable, as the overhead by creating OMP threads increases and threads get a smaller and smaller workload with every new run. All in all, the implementation is very suitable for parallelization, as there are no data-dependencies between individual pixels. For larger images, it would be therefor also worthwhile to test an MPI-implementation distributed among many nodes, possibly even for an animation of a zoom inside the Mandelbrot-set.
\begin{table}[H]
	\centering
	\begin{tabular}{|l|l|l|}
		\hline
		\textbf{OMP Threads} & \textbf{Total Time} & \textbf{MFlop/s} \\\hline
		1 & 345.002 & 2635.1 \\\hline
		2 & 178.674 & 5088.11 \\\hline
		4 & 170.495 & 5332.22 \\\hline
		8 & 116.867 & 7779.04 \\\hline
		16 & 62.2806 & 14597.1 \\\hline
		32 & 35.555 & 25569.3 \\\hline
	\end{tabular}
	\caption{Performance Metrics of Parallel Mandelbrot Solver on AMD EPYC 7763 with 8 OMP Threads}
	\label{table:strong_scaling_mandelbrot}
\end{table}
\begin{figure}[H]
	\centering
	\includegraphics[width=300px]{strong_scaling_mandelbrot.pdf}
	\caption{Strong-Scaling of parallel Mandelbrot implementation}
	\label{fig:strong_scaling_mandelbrot}
\end{figure}
\section{Bug hunt}
\begin{enumerate}
	\item The \lstinline|omp parallel for|-pragma requires the next line to be a \lstinline|for|-loop. Therefor we need to put the definition of \lstinline|tid| inside the \lstinline|for|-loop and remove the brackets \lstinline|{}| around the parallel region. When using \lstinline|omp parallel for| it will automatically parallelize the next for-loop and even if one could put the brackets around that, it would be unnecessary. The fixed code is then as follows:
	\begin{lstlisting}
#pragma omp parallel for shared(a, b, c, chunk) private(i, tid) \
schedule(static, chunk)
for (i = 0; i < N; i++) {
	tid = omp_get_thread_num();
	c[i] = a[i] + b[i];
	printf("tid= %d i= %d c[i]= %f\n", tid, i, c[i]);
}
	\end{lstlisting}
	\item This case requires, that the variable \lstinline|total| is shared among the threads. This, because if it's not explicitly set, changes of total in the \lstinline|for|-loop will not be communicated among other threads. Then in the majority of the cases we get $0$ as a result, when some other thread overwrites the total with \lstinline|total=0|. The code can therefor be simply fixed by adding the sharing of \lstinline|total| explicitly:
	\begin{lstlisting}
pragma omp shared(total) for schedule(dynamic, 10)
	\end{lstlisting}
\item In OpenMP, a section can be entered by default only by one thread. If we have, let's say three threads, then thread 0 might enter section 1 and thread 1 might enter section 2, whereas thread 2 proceeds. (This is possible, because of the \lstinline|nowait|-keyword, which indicates to the threads, that they can skip the by sections implied barrier). Now thread 0 and 1 both enter the \lstinline|print_results| function until the reach the barrier, present in this function. The issue is, that the barrier will only release, after all threads reach it, but as thread 2 never went into a section and therefor never called \lstinline|print_results|, the program will never terminate. The simplest fix is to get rid of the barrier and cope with the possibly messier output, or delete the last \lstinline|printf| altogether. There might be also other ways to have a nice output by implementing a pseudo-barrier, e.g. by using a counter, where the first threads has to wait for the second thread to finish.
\item The array \lstinline|a|, containing $1042\cdot1042=1'085'764$ doubles are private among \lstinline|nthreads| threads. This means, that for a given number of threads, the stack get's filled with $\frac{1'085'764 \cdot 8}{1024 \cdot 1024} \cdot \text{\lstinline|nthreads|} \approx 8.28\cdot\text{\lstinline|nthreads| MB}$. This can relatively fast get over the stack limit, which will cause a segmentation fault, as parts of the array are out-of-bounds. One elegant fix is to allocate the memory dynamically for every thread. This will mean, that the array will be stored on the heap instead of stack, which is much bigger. Then, when the thread finishes it's calculations, it can free up the memory to prevent memory leaks. I've chosen following implementation, which fixes the segmentation fault:
\begin{lstlisting}
pragma omp parallel shared(nthreads) private(i, j, tid)
{
	double** a = (double**)malloc(N * sizeof(double*));
	for (i = 0; i < N; i++) {
		a[i] = (double*)malloc(N * sizeof(double));
	}
	
	/* Work on the array */&
	
	for (i = 0; i < N; i++) {
		free(a[i]);
	}
	free(a);
	
} /* All threads join master thread and disband */
\end{lstlisting}
\item The deadlock occurs, if both sections simultaneously acquire both locks (\lstinline|locka| and \lstinline|lockb|). This leads to a deadlock, where both threads are locked in place and can't proceed. To prevent this from happening, locks should be generally obtained in the same order. This means, we can add a \lstinline|locka| in the second section before it locks \lstinline|lockb|. The second section then becomes:
\begin{lstlisting}
omp_set_lock(&locka);
omp_set_lock(&lockb);
for (i = 0; i < N; i++)
	b[i] = i * PI;
for (i = 0; i < N; i++)
	a[i] += b[i];
omp_unset_lock(&locka);
omp_unset_lock(&lockb);
\end{lstlisting}
Also, segmentation faults are also occuring in this project due to the size of the stack. To address that, we can allocate \lstinline|a| and \lstinline|b| dynamically (with \lstinline|malloc|) such that they are stored on the heap. For that we can add
\begin{lstlisting}
float *a = (float*)malloc(N * sizeof(float));
float *b = (float*)malloc(N * sizeof(float)); 
/* Parallel Code */
free(a); 
free(b);
\end{lstlisting} 
\noindent \emph{\textbf{Note:} All bugs were fixed and the bug-free source code can be found in the \lstinline|source/| directory of this submission. Load gcc and build the executables with \lstinline|make|. Then, the code can be run in parallel by setting \lstinline|OMP_NUM_THREADS| and running the desired binary. }
\end{enumerate}
\section{Parallel histogram calculation using \texttt{OpenMP}}
A straightforward implementation using a \lstinline|#pragma omp parallel for| will actually increase the execution time for the histogram computation. The main reason why, is false sharing. Because we don't write to \lstinline|dist| in an sequential order, but rather "randomly", the same cache-line is often needed by multiple threads. This slows them drastically down, as threads have to reload the cache-line often, when another thread invalidates the line. Therefor, we can propose a solution, where the vector is split up into equal chunks. Each thread then has a own private histogram (called \lstinline|dist_private|), which it fills with the values from it's assigned chunk. After finishing with filling the histogram, it joins a critical region, where only one thread at the time merges it's own chunk into the global \lstinline|dist|-array. This method should eliminate false sharing to the extend, such that it's worth the bottleneck, which is created by the critical region. The relevant code for the parallelization is
\begin{lstlisting}
int numThreads = omp_get_max_threads();
const int chunk = VEC_SIZE / numThreads;
#pragma omp parallel
{
  long dist_private[BINS] = {0}; //A private histogram for each thread
  #pragma omp for nowait
  for(int i = 0; i < numThreads; i++){ 
    /* Set start and end delimiters for every loop */
    for (long j = start; j < end; ++j) {
  	  dist_private[vec[j]]++;
    }
  }
  #pragma omp critical //Only one thread at a time 
  {
    for(int i = 0; i < BINS; i++){
   	  dist[i] += dist_private[i]; //Add to global histogram
    }
  } //End of critical region
} //End of parallel region
\end{lstlisting}
This code observers considerable speedup, which can be seen in Figure \ref{fig:strong_scaling_hist}.
\begin{figure}[H]
	\centering
	\includegraphics[width=300px]{strong_scaling_hist.pdf}
	\caption{Strong-Scaling of Parallel Histogram Implementation}
	\label{fig:strong_scaling_hist}
\end{figure}
Increasing the thread count from 1 to 2 or 4 massively yields considerable speedups, where as more thread still speed up the computation, but not by the same magnitude then the first few threads. This can be explained trough the bottleneck, which the critical region is. The more threads are in used, the more often the whole histogram has to be traversed such that it can be merged with the thread-private histograms. If the number of elements in the vector would be increased, we could expect the derivative of the speedup to be larger, even for more threads. Therefor, one could fine-tune a run of this application by experimenting with the number of OMP-threads. Interestingly, increasing the number of OMP threads from 32 to 40 or even 48 reduces the runtime quite strongly, more then for example increasing the number of threads from 16 to 40. However, when running on the Euler Cluster the nodes or even CPU's are not exclusive, another user can run something on the same core as well. This often leads to unexpected performance fluctuations, which I also believe to be the case here. One could address that by e.g. doing multiple runs and averaging them out.
\section{Parallel loop dependencies with \texttt{OpenMP}}
The provided code computes the sequence
\begin{align*}
	\text{opt} = [1.00000001, 1.00000001^2, 1.00000001^3, \dots , 1.00000001^n]
\end{align*}
and then outputs
\begin{align*}
	||\text{opt}||^2 = \sum_{i=1}^{n} 1.00000001^{2i} \ \text{ and } \ \text{opt}_n
\end{align*}
which are the only two values we care about. The sequential implementation has a dependency for every iterator. However we can still parallelize the code with following procedure:
\begin{enumerate}
	\item Split up opt in $n$ chunks, where $n$=number of threads
	\item Compute $m = 1.00000001^{\frac{N}{n}}$ sequentially
	\item Compute $\text{opt}_{0}, \text{opt}_{\frac{N}{n}}, \text{opt}_{2\frac{N}{n}}, \text{opt}_{3\frac{N}{n}}, \dots \text{opt}_{n-1\frac{N}{n}}$ sequentially using
	\begin{align*}
		\text{opt}_{\frac{N}{n+1}} = \text{opt}_{\frac{N}{n}} \cdot m 
	\end{align*}
	\item In parallel, complete the chunks by multiplying the previous value with $1.00000001$
\end{enumerate}
After troubleshooting quite a while with my solution and it's deviations from the sequential solution I figured out, that these differences are of numerical nature. Comparing the result of the sequential solution and the direct computation from WolframAlpha\footnote{\url{www.wolframalpha.com}} I found out, that they differ by roughly $0.00001\%$ for the last element of the sequence (\lstinline|Sn|). Therefor I'm happy with my solution, as long it doesn't differ anymore then WolframAlpha does from the sequential implementation. 

My code computes \lstinline|m| by raising $1.00000001$ to the power of \lstinline|dist|:
\begin{lstlisting}
long double m = 1.00000001;
int dist = N / num_threads;
for(int i = 0; i < dist-1; i++){
  m *= 1.00000001;
}	
\end{lstlisting}
Then it computes the starting values for every thread in sequential:
\begin{lstlisting}
for(int i = 1; i < num_threads; i++){
  opt[i*dist] = opt[(i-1)*dist] * m;
}
\end{lstlisting}
Then, the parallel computations of each chunk can start. A simple parallel region is initiated and every thread gets it's chunk to compute:
\begin{lstlisting}
#pragma omp parallel 
{
  /* Compute thread_id, start and end */
  for (int j = start+1; j < end; j++) {
    opt[j] = opt[j - 1] * up; 
  }
}
\end{lstlisting}
The parallel code has, using 48 threads, on average (10 runs) a runtime of $\approx$0.746 seconds, whereas the sequential code runs on average for ~5.505 seconds. This is a speedup of more then 7 times, quite considerable having the still quite huge sequential portion in mind.  
I then also played around with the \emph{fast exponentiation algorithm} described here\footnote{\url{https://mathstats.uncg.edu/sites/pauli/112/HTML/secfastexp.html}}. Again, averaged over 10 runs, this yielded an runtime of $\approx$0.729 seconds, only a slight, but still consistent improvement. Last but not least, I also implemented SIMD-instructions to the inner loop, where single threads compute their chunk by adding the \lstinline|#pragma omp simd| directive. This furthermore improved runtime to an average of $0.700$ seconds. This modified parallel solution was also tested on a single thread and had similar runtimes to the purely sequential solution.
\section{Quicksort using \texttt{OpenMP} tasks}
My parallel implementation creates a parallel region in the \lstinline|main|-function, before \lstinline|quicksort| is called the first time:
\begin{lstlisting}
#pragma omp parallel
{
  #pragma omp single nowait
  {
    quicksort(data, length);
  }
}
\end{lstlisting}
The \lstinline|#pragma omp single nowait| directive ensures that only one thread will execute the first recursion, but also that the other threads don't have to wait for the first task to finish (which would be at the end of the sorting). Then, inside of the \lstinline|quicksort| function threads are spawned for the left and right bin, if the remaining length is above some fixed limit:
\begin{lstlisting}
if(length > RECURSION_LENGTH){
  #pragma omp task shared(data)
  quicksort(data, right);
  #pragma omp task shared(data)
  quicksort(&(data[left]), length - left);
  #pragma omp taskwait
}else{ //Small segments don't spawn new tasks
  quicksort(data, right);
  quicksort(&(data[left]), length - left);
}
\end{lstlisting}
The directive \lstinline|#pragma omp task shared(data)| spawns a new thread for the next recursion, which shares the array \lstinline|data| with all other threads. The \lstinline|#pragma omp taskwait| is used at the end to wait for all threads to finish with their sub-bins, until the sorting can proceed. One important aspect for this implementation is the \lstinline|RECURSION_LENGTH| which will decide, how big a bin needs to be to be spawn two new threads. To find the optimal size for this, we'll choose a fixed array of size 1000000 and test it with different recursion lengths.
\begin{figure}[H]
	\centering
	\includegraphics[width=400px]{recursion_limits_1.pdf}
	\caption{Testing recursion lengths for an array of size 1000000 with 32 OMP threads for quicksort.}
	\label{fig:reucrsion_limits_1}
\end{figure}
The results of the initial test are captured in Figure \ref{fig:reucrsion_limits_1} and show, that quite small recursion limits work well with my implementation. The fastest runtime were achieved with recursion limits of around 800. This is rather surprising, as I've expected the overhead of new tasks to be larger. Even for vanishingly small recursion limits (which naturally spawns a huge amount of new tasks), the implementation performed much faster then for recursion limits greater then 2000. For understanding strong scaling we vary the list size while keeping the number of OMP-Threads fixed at 32. The list-size is varied from between 5'000 to 1'000'000 elements. 
\begin{figure}[H]
	\centering
	\includegraphics[width=400px]{strong_scaling_quicksort.pdf}
	\caption{Strong Scaling for OMP Quicksort Implementation}
	\label{fig:quicksort_strong_scaling}
\end{figure}
The Figure \ref{fig:quicksort_strong_scaling} shows a very nice strong scaling for my OMP implementation. This is to be expected, as with quicksort the size of the array nicely correlates with the amount of operations needed. The plot uses the median times among 10 runs with the variance plottet as well in a light-blue color. There are some variance spikes, which could be explained by other computations on the same CPU. All in all, the strong scaling behaves exactly as expected.
\\
\emph{Note: All launching and plotting scripts can be found in the \lstinline|source| directory of my submission.}

\end{document}
