#!/bin/bash
#SBATCH --job-name=parallel_loop_dependencies
#SBATCH --output=parallel_loop_dependencies.out
#SBATCH --error=parallel_loop_dependencies.err
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=32G

module load gcc

echo
make clean 
make
echo
echo
srun -n1 --cpus-per-task=1 --mem-per-cpu=32G recur_seq
echo 
echo "-----------------------------------"
echo 
export OMP_NUM_THREADS=1
srun -n1 --cpus-per-task=1 --mem-per-cpu=32G recur_omp
