#include <stdio.h>
#include <stdlib.h>
#include "walltime.h"
#include <omp.h>
#define DEBUG 0

void print_list(double *data, int length) {
  for (int i = 0; i < length; i++) {
    printf("%e\t", data[i]);
  }
  printf("\n");
}

void quicksort(double *data, int length, int RECURSION_LENGTH) {
  if (length <= 1) return;

  if(DEBUG){
    int omp_thread_num = omp_get_thread_num();
    if(omp_thread_num != 0){
    printf("Thread %d: ", omp_thread_num);
    }
  }

  if(RECURSION_LENGTH == -1){
    printf("Big error: RECURSION_LENGTH is not set\n");
  }

  double pivot = data[0];
  double temp;
  int left = 1;
  int right = length - 1;

  do {
    while (left  < (length - 1) && data[left ] <= pivot) left++ ;
    while (right > 0            && data[right] >= pivot) right--;

    /* swap elements */
    if (left < right) {
      temp = data[left];
      data[left ] = data[right];
      data[right] = temp;
    }
  } while (left < right);

  /* swap with pivot */
  if (data[right] < pivot) {
    data[0] = data[right];
    data[right] = pivot;
  }
 
  if(length > RECURSION_LENGTH){
    #pragma omp task shared(data)
    quicksort(data, right, RECURSION_LENGTH);
    #pragma omp task shared(data)
    quicksort(&(data[left]), length - left, RECURSION_LENGTH);
    #pragma omp taskwait
  }else{ //Small segments don't spawn new tasks
    quicksort(data, right, RECURSION_LENGTH);
    quicksort(&(data[left]), length - left, RECURSION_LENGTH);
  }
}

int check(double *data, int length) {
  for (int i = 1; i < length; i++) {
    if (data[i] < data[i-1]) return 1;
  }
  return 0;
}

int main(int argc, char **argv) {
  int length, RECURSION_LENGTH;
  double *data;

  int mem_size;

  int i, j, k;
  
    

  //length = 10000000;
  if (argc > 1){ length = atoi(argv[1]); }
  if (argc > 2){ RECURSION_LENGTH = atoi(argv[2]); }
  
  data = (double*)malloc(length * sizeof(double));
  
  if (data == NULL) {
    printf("memory allocation failed");
    return 0;
  }

  /* initialisation */
  srand(0);
  for (i = 0; i < length; i++) {
    data[i] = (double)rand() / (double)RAND_MAX;
  }

  if(0 && DEBUG){
    printf("Starting List: ");
    print_list(data, length);
  } 

  double time_start = walltime();
  #pragma omp parallel
  {
    #pragma omp single nowait
    {
      quicksort(data, length, RECURSION_LENGTH);
    }
  }
  double time = walltime() - time_start;

  if(0 && DEBUG){
    printf("Sorted List: ");
    print_list(data, length);
  } 

  printf("Size of dataset: %d, elapsed time[s] %e \n", length, time);

  if (check(data, length) != 0) printf("Quicksort incorrect.\n");

  return 0;
}
