import statistics

def calculate_median_and_variance(output_file):
    with open(output_file, 'r') as file:
        lines = file.readlines()

    recursion_limit = None
    times = []
    results = []  # Holds the recursion limit, median, and variance for each set of times

    for line in lines:
        if "LIST_SIZE=" in line:
            if recursion_limit is not None and times:
                # Calculate and store the median and variance for the previous set of times
                median_time = statistics.median(times)
                variance_time = statistics.variance(times)
                results.append((recursion_limit, median_time, variance_time))
                times = []  # Reset times for the next recursion limit
            # Update the current recursion limit
            recursion_limit = int(line.split('=')[1].strip())
        elif "elapsed time[s]" in line:
            # Extract the elapsed time in scientific notation
            try:
                time = float(line.split()[-1])
                times.append(time)
            except ValueError:
                print(f"Warning: Could not convert to float. Skipping line: {line.strip()}")

    # Process the last set of times if any
    if recursion_limit is not None and times:
        median_time = statistics.median(times)
        variance_time = statistics.variance(times)
        results.append((recursion_limit, median_time, variance_time))

    return results

def write_results_to_file(results, output_file="medians.txt"):
    with open(output_file, 'w') as file:
        for recursion_limit, median_time, variance_time in results:
            file.write(f"{recursion_limit},{median_time:.6e},{variance_time:.6e}\n")

# Adjust 'output.txt' to the path of your output file
results = calculate_median_and_variance("output.txt")
write_results_to_file(results)
