import matplotlib.pyplot as plt
import numpy as np

# Initialize lists to hold the recursion limits, median times, and variances
recursion_limits = []
median_times = []
variances = []

# Open and read the medians.txt file
with open('medians.txt', 'r') as file:
    for line in file:
        parts = line.strip().split(',')
        if(int(parts[0]) < 500000):
            recursion_limits.append(int(parts[0]))
            median_times.append(float(parts[1]))
            variances.append(float(parts[2]) * 50)  # Scale variance for visibility

# Plotting the median times as a line
plt.figure(figsize=(10, 6))

plt.plot(recursion_limits, median_times, label='Median Time')

#Use fill-between to plot the variance as a shaded region
plt.fill_between(recursion_limits, np.array(median_times) - np.array(variances), np.array(median_times) + np.array(variances), alpha=0.3, label='Variance')

plt.title('Recursion Limit Test', fontsize=16)
plt.xlabel('Recursion Limit', fontsize=14)
plt.ylabel('Time (seconds)', fontsize=14)
plt.legend()
plt.grid(True)
plt.savefig('recursion_limits.pdf')
