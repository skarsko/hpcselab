#include "walltime.h"
#include <iostream>
#include <random>
#include <vector>
#include <omp.h>

#define VEC_SIZE 1000000000
#define BINS 16

int main() {
  double time_start, time_end;

  // Initialize random number generator
  unsigned int seed = 123;
  float mean = BINS / 2.0;
  float sigma = BINS / 12.0;
  std::default_random_engine generator(seed);
  std::normal_distribution<float> distribution(mean, sigma);

  // Generate random sequence
  // Note: normal distribution is on interval [-inf; inf]
  //       we want [0; BINS-1]
  std::vector<int> vec(VEC_SIZE);
  for (long i = 0; i < VEC_SIZE; ++i) {
    vec[i] = int(distribution(generator));
    if (vec[i] < 0       ) vec[i] = 0;
    if (vec[i] > BINS - 1) vec[i] = BINS - 1;
  }

  // Initialize histogram: Set all bins to zero
  long dist[BINS];
  for (int i = 0; i < BINS; ++i) {
    dist[i] = 0;
  }

  // TODO Parallelize the histogram computation
  time_start = walltime();
  int numThreads = omp_get_max_threads();
  const int chunk = VEC_SIZE / numThreads;
  #pragma omp parallel
  {
    long dist_private[BINS] = {0}; //A private histogram for each thread
    #pragma omp for nowait
    for(int i = 0; i < numThreads; i++){ 
      int start = i * chunk;
      int end = (i + 1) * chunk;
      if(i == numThreads - 1){
        end = VEC_SIZE; //the last thread get's all the remaining elements
      }
      for (long j = start; j < end; ++j) {
        dist_private[vec[j]]++;
      }
    }
    #pragma omp critical //Only one thread at a time can update the global histogram
    {
      for(int i = 0; i < BINS; i++){
        dist[i] += dist_private[i]; //Add the private histogram to the global histogram
      }
    }
  }  
  time_end = walltime();

  // Write results
  for (int i = 0; i < BINS; ++i) {
    std::cout << "dist[" << i << "]=" << dist[i] << std::endl;
  }
  std::cout << "Time: " << time_end - time_start << " sec" << std::endl;

  return 0;
}
