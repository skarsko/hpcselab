import numpy as np

import matplotlib.pyplot as plt

# Define the number of threads and corresponding execution times
omp_threads = [1, 2, 4, 8, 12, 16, 20, 24, 28, 32, 40, 48]
execution_times = [1.74148, 1.12467, 0.649382, 0.523227, 0.440057, 0.387916, 0.441331, 0.427949, 0.355969, 0.392472, 0.313221, 0.23885] 
execution_seq = 1.71717
speedup = [execution_seq/t for t in execution_times]
fig, ax1 = plt.subplots()

# Plot execution times
ax1.plot(omp_threads, execution_times, 'b-')
ax1.set_xlabel('Number of Threads', fontsize=14)
ax1.set_ylabel('Time (s)', fontsize=14, color='b')

# Create a second y-axis for speedup
ax2 = ax1.twinx()
ax2.plot(omp_threads, speedup, 'r-')
ax2.set_ylabel('Speedup', fontsize=14, color='r')

plt.title('Strong Scaling', fontsize=16)
plt.savefig('strong_scaling.pdf')