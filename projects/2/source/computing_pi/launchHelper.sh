#!/bin/bash

# Array of desired --cpus-per-task values
cpus_per_task_values=(1 2 4 8 12 16 20 24 28 32 36 40 44 48)

# Loop over the array and submit a job for each value
for cpus in "${cpus_per_task_values[@]}"; do
    # Calculate iterations based on the number of cpus
    iters=$(($cpus * 1000000))
    
    # Use sed to modify the --cpus-per-task and iterations in scaling.sh
    # Then, submit the job with sbatch
    sed -e "s/#SBATCH --cpus-per-task=.*/#SBATCH --cpus-per-task=$cpus/" \
        -e "s/iters=[0-9]*/iters=$iters/" scaling.sh | sbatch
    #The lower line can be uncommented to run the strong scaling on a fixed number of iterations
done
