import matplotlib.pyplot as plt

# Data points
N = [1000000, 2000000, 4000000, 8000000, 16000000, 32000000, 48000000]
Threads = [1, 2, 4, 8, 16, 32, 48]
Walltiem_ser = [.00236331,.00454806,.01212243,.01916830,.03871276,.08324717,.10569133]
Walltime_crit = [.00265307,.00292116,.00314035,.01102929,.02688431,.02326438,.02015531]
Walltime_red = [.00238664,.00213660,.00234424,.00259702,.00342683,.03294082,.02595424]



# Plotting
plt.plot(N, Walltime_crit, label='Walltime [s] crititcal')
plt.plot(N, Walltime_red, label='Walltime [s] reduction')
plt.plot(N, Walltiem_ser, label='Walltime [s] serial')
plt.xlabel('N', fontsize=14)
plt.ylabel('Walltime [s]', fontsize=14)
plt.xticks(N, N)
plt.xscale('log')
plt.grid()
plt.title('Weak Scaling', fontsize=16)
plt.legend()
plt.savefig('weak_scaling.pdf')