import matplotlib.pyplot as plt

# Data points
NumThreads = [1, 2, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48]
Walltime_ser = [1.78517110, 1.92804147, 1.96890526, 2.35024121, 1.94847111, 1.94914716, 2.02989489, 1.94738378, 2.40647777, 2.13985782, 2.36413447, 1.95139912, 2.38533710, 1.98439001]
Walltime_crit = [1.50604870, 0.91856731, 0.46232403, 0.23831797, 0.33942034, 0.17229589, 0.19844515, 0.20003256, 0.16517159, 0.11460673, 0.13255374, 0.11690555, 0.09824666, 0.10346598]
Walltime_red = [1.42793370, 0.89764600, 0.44973655, 0.28065378, 0.30816316, 0.16370821, 0.18351758, 0.18505289, 0.15693426, 0.10744571, 0.14400419, 0.12668842, 0.13022804, 0.09835837]

# Speedup calculation
Speedup_crit = [Walltime_ser[0] / t for t in Walltime_crit]
Speedup_red = [Walltime_ser[0] / t for t in Walltime_red]

# Plotting
fig, ax1 = plt.subplots()

# Plotting walltime
ax1.plot(NumThreads, Walltime_crit, label='Walltime [s] crititcal')
ax1.plot(NumThreads, Walltime_red, label='Walltime [s] reduction')
ax1.plot(NumThreads, Walltime_ser, label='Walltime [s] serial')
ax1.set_xlabel('Number of Threads', fontsize=14)
ax1.set_ylabel('Walltime [s]', fontsize=14)
ax1.set_xticks(NumThreads)
ax1.grid()
ax1.set_title('Strong Scaling', fontsize=16)
ax1.legend()

# Creating a second y-axis
ax2 = ax1.twinx()

# Plotting speedup
ax2.plot(NumThreads, Speedup_crit, label='Speedup critical', linestyle='--')
ax2.plot(NumThreads, Speedup_red, label='Speedup reduction', linestyle='--')
ax2.set_ylabel('Speedup', fontsize=14)

# Displaying the legend for both axes
ax1.legend(loc='upper left')
ax2.legend(loc='upper right')

# Saving the plot
plt.savefig('strong_scaling_improved.pdf')
