#include <stdlib.h> /* atol */
#include <stdio.h>
#include "walltime.h"


int main(int argc, char *argv[]) {
  long int N = 1000000;
  double time_start, h, sum, pi;

  if ( argc > 1 ) N = atol(argv[1]);

  /* Parallelize with OpenMP using the critical directive */
  time_start = walltime();
  h = 1./N;
  sum = 0.;

  #pragma omp parallel
  {
    double sumP = 0.;
    int numThreads = omp_get_num_threads();
    int threadId = omp_get_thread_num();
    int i = threadId * N / numThreads;
    int iL = (threadId + 1) * N / numThreads;
    if(threadId == numThreads - 1) iL = N;
    for (; i < iL; ++i) {
     double x = (i + 0.5)*h;
     sumP += 4.0 / (1.0 + x*x);
    }
    #pragma omp critical
    sum += sumP;
  }
  pi = sum*h;
  double time = walltime() - time_start;

  printf("pi = \%.15f, N = %9d, time = %.8f secs\n", pi, N, time);

  return 0;
}
