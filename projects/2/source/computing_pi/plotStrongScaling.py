import matplotlib.pyplot as plt

# Data points
NumThreads = [1,2,4,8,16,32,48]
Walltiem_ser = [.05622339,.05647587,.07546324,.07024391,.06815489,.06604297,.08593073]
Walltime_crit = [.05613178,.04940684,.01897874,.00990577,.00692436,.02734317,.02460850]
Walltime_red = [.05624762,.06961638,.01897892,.00983166,.01096758,.01128553,.02499132]




# Plotting
plt.plot(NumThreads, Walltime_crit, label='Walltime [s] crititcal')
plt.plot(NumThreads, Walltime_red, label='Walltime [s] reduction')
plt.plot(NumThreads, Walltiem_ser, label='Walltime [s] serial')
plt.xlabel('Number of Threads', fontsize=14)
plt.xscale('log')
plt.ylabel('Walltime [s]', fontsize=14)
plt.xticks(NumThreads, NumThreads)
plt.grid()
plt.title('Strong Scaling', fontsize=16)
plt.legend()
plt.savefig('strong_scaling.pdf')