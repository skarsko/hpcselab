import matplotlib.pyplot as plt

# Data
omp_threads = [1, 2, 4, 8, 16, 32]
total_time = [345.002, 178.674, 170.495, 116.867, 62.2806, 35.555]
mflops = [2635.1, 5088.11, 5332.22, 7779.04, 14597.1, 25569.3]
log_omp_threads = [0, 1, 2, 3, 4, 5]

fig, ax1 = plt.subplots()

ax1.plot(log_omp_threads, total_time, 'b-', label='Total Time')
ax1.set_xlabel('OMP Threads', fontsize=12)
ax1.set_ylabel('Time (s)', fontsize=12)


ax2 = ax1.twinx()
ax2.plot(log_omp_threads, mflops, 'r-', label='MFlops')
ax2.set_ylabel('MFlop/s', fontsize=12)
ax2.set_xticks(log_omp_threads)
ax2.set_xticklabels(omp_threads)


ax1.legend(loc='upper left')
ax2.legend(loc='upper right')
plt.title('Strong Scaling of Mandelbrot', fontsize=16)

plt.tight_layout()
plt.savefig('strongScaling.pdf')
