/*
*
Poisson equation solver using finite difference method and PETSc library.
HPCSELab 2024, Submission by Samuel Karsko
*
*/

//TODO: Remove unnecessary includes
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmat.h>
#include <iostream>
#include <chrono>
#include "matplotlibcpp.h"
namespace plt = matplotlibcpp;

#define DEBUG 0 //Prints the solution to the console
#define GRID_SIZE 500 //Square grid
#define VISUALIZE 1 //Plot the results to poisson.pdf
#define ONLY_TIMING 0 //Only print the time taken, useful for scaling analysis

static char help[] = "Solves the Poisson equation using finite difference method.\n";
void displayTitle();
void printMatrix(PetscInt M, PetscInt N, PetscScalar **array);

int main(int argc, char **argv) {
    //Time the execution
    auto start = std::chrono::high_resolution_clock::now();

    //Define PETSc variables
    PetscErrorCode ierr;
    DM da; // DMDA object
    Vec u, b, u_global;
    Mat A;
    KSP ksp;
    PC pc;
    PetscInt i, j, M = GRID_SIZE, N = GRID_SIZE, xs, ys, xm, ym, its;
    PetscReal hx, hy, hxdhy, hydhx, value;
    PetscScalar **array_u, **array_b;
    int rank, size;

    // Initialize PETSc and MPI
    ierr = PetscInitialize(&argc, &argv, (char*)0, help); CHKERRQ(ierr);
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &rank));
    PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD, &size));
    if(!rank && !ONLY_TIMING){
        displayTitle();
        std::cout << "Solving the Poisson equation using finite difference method." << std::endl;
        std::cout << "Grid size: " << M << "x" << N << std::endl;
        if(DEBUG){
            std::cout << "Debug mode is enabled." << std::endl;
        }
        if(VISUALIZE){
            std::cout << "Visualization is enabled." << std::endl;
        }
        std::cout << "The simulation will be run on " << size << " processes." << std::endl;
        std::cout << "\n=====================================================" << std::endl;
    }

    // Create a 2D DMDA grid
    ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, M, N, \
    1, size, 1, 1, NULL, NULL, &da); CHKERRQ(ierr);
    ierr = DMSetFromOptions(da); CHKERRQ(ierr);
    ierr = DMSetUp(da); CHKERRQ(ierr);

    // Create global vectors for the solution and right-hand side
    ierr = DMCreateGlobalVector(da, &u); CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(da, &b); CHKERRQ(ierr);

    // Create matrix A
    ierr = DMCreateMatrix(da, &A); CHKERRQ(ierr);

    // Get local grid boundaries and save them in xs, ys, xm, ym
    ierr = DMDAGetCorners(da, &xs, &ys, 0, &xm, &ym, 0); CHKERRQ(ierr);
    if(DEBUG && !ONLY_TIMING){
        std::cout << "Rank " << rank << " has grid boundaries: " << xs << ", " << ys << ", " << xm << ", " << ym << std::endl;
    }
    
    //hx and hy are the grid spacings, equally spaced in x and y
    hx = 1.0 / (M-1);
    hy = 1.0 / (N-1);
    hxdhy = hx / hy;
    hydhx = hy / hx;
    MatStencil   row, col[5];
    PetscScalar v[5];
    
    // Fill matrix A and vector b
    !ONLY_TIMING && rank == 0 && std::cout << "Filling matrix A and vector b..." << std::endl;
    for (j = ys; j < ys + ym; j++) {
        for (i = xs; i < xs + xm; i++) {
            row.i = i;
            row.j = j;
            PetscInt global_index = i + j * M;
            if (i == 0 || j == 0 || i == M-1 || j == N-1) {
                // Boundary conditions
                value = 1.0;
                // Set the diagonal element of A to 1
                ierr = MatSetValuesStencil(A, 1, &row, 1, &row, &value, INSERT_VALUES); CHKERRQ(ierr);
                // Dirichlet boundary conditions
                value = 0.0;
                ierr = VecSetValues(b, 1, &global_index, &value, INSERT_VALUES); CHKERRQ(ierr);
            } else {
                // Interior points, according to the finite difference method
                col[0].i = i; 
                col[0].j = j; 
                v[0] = -2.0 * (hxdhy + hydhx);

                col[1].i = i; 
                col[1].j = j - 1; 
                v[1] = hxdhy;
                
                col[2].i = i; 
                col[2].j = j + 1; 
                v[2] = hxdhy;

                col[3].i = i - 1; 
                col[3].j = j; 
                v[3] = hydhx;

                col[4].i = i + 1; 
                col[4].j = j; 
                v[4] = hydhx;

                ierr = MatSetValuesStencil(A, 1, &row, 5, col, v, INSERT_VALUES); CHKERRQ(ierr);

                // Set the right-hand side to -20*hx*hy
                value = -20.0 * hx * hy;
                ierr = VecSetValues(b, 1, &global_index, &value, INSERT_VALUES); CHKERRQ(ierr);
            }
        }
    }
    
    // Wait for all processes to finish
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));

    // Assemble matrix and vector
    ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = VecAssemblyBegin(b); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(b); CHKERRQ(ierr);

    !ONLY_TIMING && rank == 0 && std::cout << "Solving system..." << std::endl;
   
    // Linear solver using KSP and Jacobi preconditioner
    ierr = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp, A, A); CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp, &pc); CHKERRQ(ierr);
    ierr = PCSetType(pc, PCJACOBI); CHKERRQ(ierr);

    // Solve the system
    ierr = KSPSolve(ksp, b, u); CHKERRQ(ierr);
    ierr = KSPGetIterationNumber(ksp, &its); CHKERRQ(ierr);

    // Output solution
    ierr = DMDAVecGetArray(da, u, &array_u); CHKERRQ(ierr);
    ierr = DMDAVecGetArray(da, b, &array_b); CHKERRQ(ierr);
    auto end = std::chrono::high_resolution_clock::now();
    #if DEBUG
    for (j = ys; j < ys + ym; j++) {
        for (i = xs; i < xs + xm; i++) {
            std::cout << "u[" << i << "," << j << "] = " << array_u[j][i] << std::endl;
        }
    }
    #endif
    !ONLY_TIMING && rank == 0 && std::cout << "=====================================================\n" << std::endl;
    !ONLY_TIMING && rank == 0 && std::cout << "The solver took " << its << " iterations to converge." << std::endl;

    // Scatter the solution to a global vector
    VecScatter scatter;
    ierr = VecCreateSeq(PETSC_COMM_SELF, M * N, &u_global); CHKERRQ(ierr);
    ierr = VecScatterCreateToZero(u, &scatter, &u_global); CHKERRQ(ierr);
    ierr = VecScatterBegin(scatter, u, u_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, u, u_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scatter); CHKERRQ(ierr);

    PetscScalar* array_global;
    ierr = VecGetArray(u_global, &array_global); CHKERRQ(ierr);

    // Visualize using matplotlibcpp
    #if VISUALIZE
    PetscCallMPI(MPI_Barrier(PETSC_COMM_WORLD));
    !ONLY_TIMING && rank == 0 && std::cout << "Visualizing solution..." << std::endl;
    if(rank == 0 && !ONLY_TIMING){
        
        std::vector<std::vector<double>> z(M, std::vector<double>(N, 0.0));
        for (j = 0; j < N; j++) {
            for (i = 0; i < M; i++) {
                
                    z[j][i] = array_global[i + j * M];
                
            }
        }
        plt::imshow(z);
        plt::colorbar();
        std::vector<double> ticks = {0.0, 0.2, 0.4, 0.6, 0.8, 1.0};
        std::vector<std::string> empty_labels(ticks.size(), "");

        plt::title("Solution to Poisson Equation");
        plt::savefig("poisson.pdf");
        rank == 0 && std::cout << "The solution has been saved to poisson.pdf." << std::endl;
        
    }
    #endif

    ierr = DMDAVecRestoreArray(da, u, &array_u); CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(da, b, &array_b); CHKERRQ(ierr);

    // Free memory
    ierr = KSPDestroy(&ksp); CHKERRQ(ierr);
    ierr = VecDestroy(&u); CHKERRQ(ierr);
    ierr = VecDestroy(&b); CHKERRQ(ierr);
    ierr = VecDestroy(&u_global); CHKERRQ(ierr);
    ierr = MatDestroy(&A); CHKERRQ(ierr);
    ierr = DMDestroy(&da); CHKERRQ(ierr);
    ierr = PetscFinalize();

    // Print how long the simulation took
    std::chrono::duration<double> elapsed = end - start;
    rank == 0 && std::cout << "The simulation took " << elapsed.count() << " seconds." << std::endl;

    return ierr;
}


// Cool title
void displayTitle(){
    std::cout << R"(
        
 _______  _______  _______ _________ _______  _______  _      _         
(  ____ )(  ___  )\__   __/(  ____ \(  ____ \(  ___  )( (    /|
| (    )|| (   ) |   ) (   | (    \/| (    \/| (   ) ||  \  ( |
| (____)|| |   | |   | |   | (_____ | (_____ | |   | ||   \ | |
|  _____)| |   | |   | |   (_____  )(_____  )| |   | || (\ \) |
| (      | |   | |   | |         ) |      ) || |   | || | \   |
| )      | (___) |___) (___/\____) |/\____) || (___) || )  \  |
|/       (_______)\_______/\_______)\_______)(_______)|/    )_)
                                                               
     _______  _______  _               _______  _______        
    (  ____ \(  ___  )( \    |\     /|(  ____ \(  ____ )       
    | (    \/| (   ) || (    | )   ( || (    \/| (    )|       
    | (_____ | |   | || |    | |   | || (__    | (____)|       
    (_____  )| |   | || |    ( (   ) )|  __)   |     __)       
          ) || |   | || |     \ \_/ / | (      | (\ (          
    /\____) || (___) || (____/\\   /  | (____/\| ) \ \__       
    \_______)(_______)(_______/ \_/   (_______/|/   \__/       
                                         
                                                               
    )" << std::endl;
}

//Debug function to print a matrix
void printMatrix(PetscInt M, PetscInt N, PetscScalar **array){
    for (int j = 0; j < N; j++) {
        for (int i = 0; i < M; i++) {
            std::cout << array[j][i] << " ";
        }
        std::cout << std::endl;
    }
}