import matplotlib.pyplot as plt

#Visuals
plt.style.use('seaborn-v0_8-dark')


mpi_tasks = [1, 2, 4, 8, 16, 32]

# Data
ex200 = [1.8364, 1.33277, 1.19859, 1.15329, 1.50836, 2.17957]
ex300 = [7.48927, 4.17955, 3.22065, 2.60972, 2.41733, 2.67124]
ex400 = [22.0171, 11.0685, 6.22947, 4.34757, 3.78445, 4.02428]


fig, ax1 = plt.subplots()

# Execution time
ax1.plot(mpi_tasks, ex200, 'o-', label='200x200')
ax1.plot(mpi_tasks, ex300, 'o-', label='300x300')
ax1.plot(mpi_tasks, ex400, 'o-', label='400x400')

ax1.set_xlabel('Number of MPI Tasks')
ax1.set_ylabel('Execution Time (s)')

#Make x-axis log scale
ax1.set_xscale('log', base=2)


plt.title('Strong Scaling of Poisson Solver')

# Legend
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = [], []
ax1.legend(lines + lines2, labels + labels2, loc='upper right')

# Displaying the plot
plt.savefig('strong_scaling.pdf')
