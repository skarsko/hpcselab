#!/bin/bash

POISSON_APP="./poisson"
TASKS=(1 2 4 8 16 32)
repetitions=5

source sourceme.sh
make clean && make
echo "Gridsize: 500x500"
echo
for TASK in "${TASKS[@]}"
do
    for (( i=1; i<=$repetitions; i++ ))
    do
        echo "Running with $TASK tasks ($i/$repetitions)"
        mpirun -np $TASK $POISSON_APP
        echo "-----------------------------------"
    done
done