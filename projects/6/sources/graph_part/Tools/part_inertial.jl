# M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    inertial_part(A, coords)

Compute the bi-partions of graph `A` using inertial method based on the
`coords` of the graph.

# Examples
```julia-repl
julia> inertial_part(A, coords)
 1
 ⋮
 2
```
"""
function inertial_part(A, coords)

    # 1. Compute the center of mass.
    x_c = sum(coords[:,1])/size(coords)[1]
    y_c = sum(coords[:,2])/size(coords)[1]
    # 2. Construct the matrix M. (see pdf of the assignment)
    Syy = 0
    Sxy = 0
    Sxx = 0
    for i in 1:size(coords)[1]
        Syy += (coords[i,2] - y_c)^2
        Sxy += (coords[i,1] - x_c)*(coords[i,2] - y_c)
        Sxx += (coords[i,1] - x_c)^2
    end
    M = [Syy -Sxy; -Sxy Sxx]
    # 3. Compute the eigenvector associated with the smallest eigenvalue of M.
    eigv = eigvecs(M)
    # 4. Partition the nodes around line L 
    #    (use may use the function partition(coords, eigv))
    p1, p2 = partition(coords, eigv[:,1])
    # 5. Return the indicator vector
    p = ones(Int, size(coords)[1])
    p[p1] .= 1
    p[p2] .= 2
    return p

end
