# M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    spectral_part(A, fiedler=false)

Compute the bi-partions of graph `A` using spectral method.

If `fiedler` is true, return the entries of the fiedler vector.

# Examples
```julia-repl
julia> spectral_part(A)
 1
 ⋮
 2
```
"""
function spectral_part(A, fiedler=false)
    n = size(A)[1]

    if n > 4*10^4
        @warn "graph is large. Computing eigen values may take too long."     
    end

    # 1. Construct the Laplacian matrix.
    D = Diagonal(sum(A, dims=2)[:])
    L = D - A
    # 2. Compute its eigendecomposition.
    tol = 1e-6
    retries = 5
    success = false
    maxiter, ncv = 1000, 10
    λ, U = [], []

    for _ in 1:retries
        try
            λ, U = eigs(L, nev=2, which=:SM, maxiter=maxiter, ncv=ncv, tol=tol)
            success = true
            break
        catch e
            @warn "Eigenvalue computation did not converge, retrying..."
            maxiter *= 2  
            ncv += 2  
        end
    end
    # 3. Label the vertices with the entries of the Fiedler vector.
    u2 = U[:, 2]  # the second smallest eigenvector
    if fiedler
        return u2
    end
    # 4. Partition them around their median value, or 0.
    ε = median(u2)  # using the median of u2
    V1 = findall(u -> u < ε, u2)
    V2 = findall(u -> u >= ε, u2)
    # 5. Return the indicator vector
    p = zeros(Int, n)
    p[V1] .= 1
    p[V2] .= 2
    return p

end
