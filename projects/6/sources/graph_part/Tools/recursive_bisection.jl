# M.L. for High Performance Computing Lab @USI & @ETHZ - malik.lechekhab@usi.ch 
"""
    rec_bisection(method, levels, A, coords=zeros(0), vn=zeros(0))

Compute recursive partitioning of graph `A` using a specified `method` and
number of `levels`.

If the `method` is `coords`-based, coordinates must be passed.

# Examples
```julia-repl
julia> rec_bisection("spectralPart", 3, A)
 5
 ⋮
 2

julia> rec_bisection("coordinatePart", 3, A, coords)
 1
 ⋮
 8
```
"""
function rec_bisection(method, levels, A, coords=zeros(0), vn=zeros(0))
    p = zeros(Int, size(A)[1])
    p = recursion(method, levels, A, coords, 1)    
    return p
end

function recursion(method, levels, A, coords, index)
    if levels%2 == 0
        levels /= 2
        if method == "spectralPart"
            p = spectral_part(A)
        elseif method == "coordinatePart"
            p = coordinate_part(A, coords)
        else
            error("Method not found")
        end

end


