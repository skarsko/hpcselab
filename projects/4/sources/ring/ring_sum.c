#include <mpi.h> // MPI
#include <stdio.h>

int main(int argc, char *argv[]) {

  // Initialize MPI, get size and rank
  int size, rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  // IMPLEMENT: Ring sum algorithm
  int sum = 0; // initialize sum

  int sendTo = (rank + 1) % size; 
  int getFrom = (rank - 1);
  if(getFrom < 0) getFrom = size - 1;

  int sendValue = rank;
  int recvValue = 0;

  for(int i = 0; i < size; i++){
    //MPI_Sendrecv to avoid deadlock
    MPI_Sendrecv(&sendValue, 1, MPI_INT, sendTo, 0, \
                 &recvValue, 1, MPI_INT, getFrom, 0, \
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    sendValue = recvValue;
    sum += recvValue;
  }

  printf("Rank %d: Sum = %d\n", rank, sum);
  // Finalize MPI
  MPI_Finalize();

  return 0;
}
