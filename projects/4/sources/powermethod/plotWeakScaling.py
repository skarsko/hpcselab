import matplotlib.pyplot as plt

#Visuals
plt.style.use('seaborn-v0_8-dark')


mpi_tasks = [1, 2, 4, 8, 16, 32, 64]

# Data
#Execution times on 1 node
#execution_time = [3.303809,
#3.343698,
#4.95837,
#4.475307,
#4.408572,
#4.680706,
#13.985734]

#Execution times on 64 nodes
execution_time = [3.085761,
4.615963,
4.959031,
5.394538,
5.906694,
6.487103,
6.775388]
speedup = [execution_time[0] / t for t in execution_time]

fig, ax1 = plt.subplots()

# Execution time
ax1.plot(mpi_tasks, execution_time, marker='o', color='b', label='Execution Time')
ax1.set_xlabel('Number of MPI Tasks')
ax1.set_ylabel('Execution Time (s)', color='b')
ax1.tick_params(axis='y', labelcolor='b')


ax1.set_xscale('log')


# Speedup 
ax2 = ax1.twinx()
ax2.plot(mpi_tasks, speedup, marker='o', color='r', label='Speedup')
ax2.set_ylabel('Speedup', color='r')
ax2.tick_params(axis='y', labelcolor='r')


#Set the x-axis ticks to 1,2,4,8,16,32,64
ax1.set_xticks(mpi_tasks)
ax1.get_xaxis().set_major_formatter(plt.ScalarFormatter())

# Plot parameters
#ax1.set_ylim([0,10])
ax2.set_ylim([min(speedup)-0.5, max(speedup)+4])

#plt.title('Weak Scaling of Parallel Power Method (All MPI Tasks on Single Node)')
plt.title('Weak Scaling of Parallel Power Method (All MPI Tasks on Separate Nodes)')

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax1.legend(lines + lines2, labels + labels2, loc='upper left')

# Displaying the plot
plt.savefig('weak_scaling.pdf')