/****************************************************************
 *                                                              *
 * This file has been written as a sample solution to an        *
 * exercise in a course given at the CSCS-USI Summer School     *
 * It is made freely available with the understanding that      *
 * every copy of this file must include this header and that    *
 * CSCS/USI take no responsibility for the use of the enclosed  *
 * teaching material.                                           *
 *                                                              *
 * Purpose: Exchange ghost cell in 2 directions using a topology*
 *                                                              *
 * Contents: C-Source                                           *
 *                                                              *
 ****************************************************************/

/* Use only 16 processes for this exercise
 * Send the ghost cell in two directions: left<->right and top<->bottom
 * ranks are connected in a cyclic manner, for instance, rank 0 and 12 are connected
 *
 * process decomposition on 4*4 grid
 *
 * |-----------|
 * | 0| 1| 2| 3|
 * |-----------|
 * | 4| 5| 6| 7|
 * |-----------|
 * | 8| 9|10|11|
 * |-----------|
 * |12|13|14|15|
 * |-----------|
 *
 * Each process works on a 6*6 (SUBDOMAIN) block of data
 * the D corresponds to data, g corresponds to "ghost cells"
 * xggggggggggx
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * gDDDDDDDDDDg
 * xggggggggggx
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define SUBDOMAIN 6
#define DOMAINSIZE (SUBDOMAIN+2)

int main(int argc, char *argv[])
{
    int rank, size, i, j, dims[2], periods[2], rank_top, rank_bottom, rank_left, rank_right;
    double data[DOMAINSIZE*DOMAINSIZE];
    MPI_Request request;
    MPI_Status status;
    MPI_Comm comm_cart;
    MPI_Datatype data_ghost;

    // Initialize MPI
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size!=16) {
        printf("please run this with 16 processors\n");
        MPI_Finalize();
        exit(1);
    }

    // initialize the domain
    for (i=0; i<DOMAINSIZE*DOMAINSIZE; i++) {
        data[i]=rank;
    }

    //Dimensions and periods for the cartesian communicator
    dims[0]=4; // 4*4 grid
    dims[1]=4;
    periods[0]=1; // periodic boundaries
    periods[1]=1;

    //Create cartesian communicator
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &comm_cart);
    
    //Find neighbours
    MPI_Cart_shift(comm_cart, 0, 1, &rank_top, &rank_bottom);
    MPI_Cart_shift(comm_cart, 1, 1, &rank_left, &rank_right);

    //Type vector for columns with row-stride
    MPI_Type_vector(SUBDOMAIN, 1, DOMAINSIZE, MPI_DOUBLE, &data_ghost);
    MPI_Type_commit(&data_ghost);

    //Data preparation for communication
    double *send_bottom = \
        &data[DOMAINSIZE*DOMAINSIZE - DOMAINSIZE - SUBDOMAIN - 1]; // Last real row
    
    double *recv_top = &data[1]; // Top ghost row
    
    double* send_top = &data[SUBDOMAIN+1]; // First real row
    double* recv_bottom = \
        &data[DOMAINSIZE*DOMAINSIZE - DOMAINSIZE + 1]; // Bottom ghost row
    
    double* send_left = &data[DOMAINSIZE]; // First real column
    double* recv_right = &data[DOMAINSIZE+SUBDOMAIN+1]; // Right ghost column

    double* send_right = &data[DOMAINSIZE+SUBDOMAIN]; // Last real column
    double* recv_left = &data[DOMAINSIZE]; // Left ghost column

    int num_comm = 8;
    MPI_Request requests[num_comm];
    MPI_Status statuses[num_comm];

    //Ghost cell exchange
    MPI_Isend(send_bottom, SUBDOMAIN, MPI_DOUBLE, rank_bottom, 0, comm_cart, &requests[0]);
    MPI_Isend(send_top, SUBDOMAIN, MPI_DOUBLE, rank_top, 0, comm_cart, &requests[1]);
    MPI_Isend(send_left, 1, data_ghost, rank_left, 0, comm_cart, &requests[2]);
    MPI_Isend(send_right, 1, data_ghost, rank_right, 0, comm_cart, &requests[3]);

    MPI_Irecv(recv_top, SUBDOMAIN, MPI_DOUBLE, rank_top, 0, comm_cart, &requests[4]);
    MPI_Irecv(recv_bottom, SUBDOMAIN, MPI_DOUBLE, rank_bottom, 0, comm_cart, &requests[5]);
    MPI_Irecv(recv_right, 1, data_ghost, rank_right, 0, comm_cart, &requests[6]);
    MPI_Irecv(recv_left, 1, data_ghost, rank_left, 0, comm_cart, &requests[7]);
   
    MPI_Waitall(num_comm, requests, statuses);

    //Check for errors
    for(int i = 0; i < num_comm; i++) {
        if(statuses[i].MPI_ERROR != MPI_SUCCESS) {
            printf("Error in communication\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }

    if (rank==9) {
        printf("data of rank 9 after communication\n");
        for (j=0; j<DOMAINSIZE; j++) {
            for (i=0; i<DOMAINSIZE; i++) {
                printf("%.1f ", data[i+j*DOMAINSIZE]);
            }
            printf("\n");
        }
    }

    // Free MPI resources (e.g., types and communicators)
    MPI_Type_free(&data_ghost);
    MPI_Comm_free(&comm_cart);

    // Finalize MPI
    MPI_Finalize();

    return 0;
}
