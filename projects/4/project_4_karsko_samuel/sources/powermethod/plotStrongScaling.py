import matplotlib.pyplot as plt

#Visuals
plt.style.use('seaborn-v0_8-dark')


mpi_tasks = [1, 2, 4, 8, 16, 32, 64]

# Data
execution_time = [5*60+10, 3*60+13, 1*60+46, 53, 26, 19, 16]
speedup = [execution_time[0] / t for t in execution_time]

fig, ax1 = plt.subplots()

# Execution time
ax1.plot(mpi_tasks, execution_time, marker='o', color='b', label='Execution Time')
ax1.set_xlabel('Number of MPI Tasks')
ax1.set_ylabel('Execution Time (s)', color='b')
ax1.tick_params(axis='y', labelcolor='b')

# Speedup 
ax2 = ax1.twinx()
ax2.plot(mpi_tasks, speedup, marker='o', color='r', label='Speedup')
ax2.set_ylabel('Speedup', color='r')
ax2.tick_params(axis='y', labelcolor='r')


# Plot parameters
ax2.set_ylim([min(speedup)-0.5, max(speedup)+4])

plt.title('Strong Scaling of Parallel Power Method (All MPI Tasks on Different Nodes)')

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax1.legend(lines + lines2, labels + labels2, loc='upper right')

# Displaying the plot
plt.savefig('strong_scaling.pdf')