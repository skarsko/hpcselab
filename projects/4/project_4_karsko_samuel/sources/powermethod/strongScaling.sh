#!/bin/bash

ml gcc openmpi 

make clean
make


num_procs=64
matrix=3
n=10000
iters=3000
tol=-1


# Loop over the number of processors
for ((procs=2; procs<=num_procs; procs*=2))
do
    srun --mpi=pmi2 --ntasks=$procs ./powermethod_rows $matrix $n $iters $tol
    echo 
    echo "------------------------------------"
    echo
done