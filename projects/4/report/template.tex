\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\input{assignment.sty}


%Custom Code
\usepackage{listings}
\usepackage{sourcecodepro}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{amsmath}
\usepackage{float}
\usepackage{makecell}

\graphicspath{{images/}} 

\lstset{basicstyle=\footnotesize\ttfamily, language=bash, showstringspaces=false}
\lstset{frame=single}
\lstset{
	showlines=true
}


\newcommand{\br}{\vspace{8pt}\\}
\newcommand{\brr}{\vspace{8pt}}
%End Custom Code


\begin{document}


\setassignment
\setduedate{Monday 29 April 2024, 23:59 (midnight).}

\serieheader{High-Performance Computing Lab for CSE}{2024}
            {Student: Samuel Karsko}
            {Discussed with: -}{Solution for Project 4}{}
\newline

\section{Ring sum using MPI}
The ring sum can be computed MPI by sending messages containing their accumulated sum to their neighbour, while receiving such message from the other neighbour. This is repeated $n$ times, where $n$ is the number of tasks forming the ring. We can implement this, by iterating $n$ times, and in every iteration, every rank sends a \lstinline|MPI_Sendrecv| to its neighbour:
\begin{lstlisting}
for(int i = 0; i < size; i++){
  MPI_Sendrecv(&sendValue, 1, MPI_INT, sendTo, 0, \
  &recvValue, 1, MPI_INT, getFrom, 0, \
  MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  
  sendValue = recvValue;
  sum += recvValue;
}
\end{lstlisting}
By using \lstinline|MPI_Sendrecv| instead of a separate send and receive, we can eliminate the risk of deadlocks, as every task will naturally wait for it's neighbour, and only if all threads are ready to send and receive, the messages will go out at the same time. Also, the use of \lstinline|MPI_Sendrecv| is the more elegant approach in such cyclic dependencies and indicate clearly what's happening to the reader of the code.
\section{Cartesian domain decomposition and ghost cells exchange}
For ghost cell exchange with MPI communication we first need a cartesian communicator, which can be luckily initialized with \lstinline|MPI_Cart_create| with the dimensions and periodicity as arguments. The neighbouring ranks can be also found with the useful \lstinline|MPI_Cart_shift| functionality and saved into respective variables. For the access of columns, we use a stride-based accessor, which can be initialized with
\begin{lstlisting}
MPI_Type_vector(SUBDOMAIN, 1, DOMAINSIZE, MPI_DOUBLE, &data_ghost);
MPI_Type_commit(&data_ghost);	
\end{lstlisting} 
such that we can use \lstinline|data_ghost| in communication. Then, pointers to the 4 inner (real row/column) and 4 outer (ghost row/column) edge points of the grid can be set and also saved as variables for the communication: 
\begin{lstlisting}
double *send_bottom = &data[DOMAINSIZE*DOMAINSIZE - DOMAINSIZE - SUBDOMAIN - 1]; 
double *recv_top = &data[1]; 
double* send_top = &data[SUBDOMAIN+1]; 
double* recv_bottom = &data[DOMAINSIZE*DOMAINSIZE - DOMAINSIZE + 1]; 
double* send_left = &data[DOMAINSIZE]; 
double* recv_right = &data[DOMAINSIZE+SUBDOMAIN+1];
double* send_right = &data[DOMAINSIZE+SUBDOMAIN]; 
double* recv_left = &data[DOMAINSIZE]; 
\end{lstlisting}
Finally, arrays for the statuses and requests for the communications can be initialized and the communication can start. The vertical communication (row-based) accesses the values directly from the array \lstinline|data| whereas the horizontal (column-based) communication uses the \lstinline|data_ghost| derived data type:
\begin{lstlisting}
//Send all messages
MPI_Isend(send_bottom, SUBDOMAIN, MPI_DOUBLE, rank_bottom, 0, comm_cart, &requests[0]);
...
MPI_Isend(send_right, 1, data_ghost, rank_right, 0, comm_cart, &requests[3]);

//Recieve all messages
MPI_Irecv(recv_top, SUBDOMAIN, MPI_DOUBLE, rank_top, 0, comm_cart, &requests[4]);
...
MPI_Irecv(recv_left, 1, data_ghost, rank_left, 0, comm_cart, &requests[7]);
\end{lstlisting}
Then, a \lstinline|MPI_Waitall| is initialized, which waits until all non-blocking operations are complete. The fact, that all sends come before receives can eliminate the risk of a deadlock. Finally, the MPI resources (\lstinline|data_ghost| and \lstinline|comm_cart|) are cleaned up, and the MPI-segment finalized. 
\section{Parallelizing the Mandelbrot set using MPI [30 Points]}
Parallelization can be quite challenging if domains need to be decomposed and synchronized again. Luckily, MPI provides in-built functionality for grids, which enable reliable and consistent results. In the function \lstinline|createPartition|, we can let the first rank create the division of MPI tasks on the grid using \lstinline|MPI_Dims_create| and then broadcast said dimensions to all other processes. Then a Cartesian communicator \lstinline|p.comm| is created which is then used to determine the coordinates of every process on the grid. The \lstinline|updatePartition| function on the other hand is used when synchronizing the data and can be used to update an existing partition struct \lstinline|p_old| into the partition of the process with rank \lstinline|mpi_rank|. Here we reuse the grid dimension and communicator of the old process \lstinline|p_old| and get the coordinates of the new MPI process by invoking \lstinline|MPI_Cart_coords| again. Finally, the function \lstinline|createDomain| returns a struct of type \lstinline|Domain|, which holds the local size of the domain (to be computed by a single process) and it's starting and ending point in the context of the larger grid. For that, we can simply divide the image by the number of processes in $x$ and $y$ direction and compute the starting and endpoints using the local width and height of the domain, which one processor should compute:
\begin{lstlisting}
Domain d;

//Grid width and height of local domain
d.nx = IMAGE_WIDTH / p.nx;
d.ny = IMAGE_HEIGHT / p.ny;

//Starting point for current process
d.startx = p.x * d.nx;
d.starty = p.y * d.ny;

//End point for current process
d.endx = d.startx + d.nx;
d.endy = d.starty + d.ny;
\end{lstlisting}
Finally, the first rank needs to collect the information from all processors, once they are all finished. For that we first send the local partition \lstinline|c| from all processes (except the first) to the master process with \lstinline|MPI_Send|. The master process will then collect this local partition using \lstinline|MPI_Recv|, which will receive the partition of the process \lstinline|proc| and save it into the temporary array \lstinline|c|. This array is then added to the \lstinline|png_plot| function which will create the visualization of the Mandelbrot set.

Using the provided performance and plotting script we can visualize the runtimes of all single tasks, given a varying number of total MPI tasks in a neat plot, seen in Figure \ref{mandelbrot_plot}.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{plot.pdf}
	\caption{Performance of MPI Mandelbrot set implementation with varying total number of MPI tasks}
	\label{mandelbrot_plot}
\end{figure}
First of all, we see in Figure \ref{mandelbrot_plot}, that for less MPI processes, the workload per MPI process is larger, which shows, that the division of workload among MPI processes works. However, we see that for most of the multi-processes runs, the middle or latter processes have a much higher workload, then the first few ones. This is likely due to the fact, that we split the domain in equal parts, without considering, that different parts of the Mandelbrot set require much more work then others. This is particularly important in a Mandelbrot set, where (fast) convergence can lead to a much faster computation compared to diverging pixels, which will run until the maximum iteration. One possible solution for this issue would be to let a process, which finished with it's part work on a different partition. This would require splitting the yet unfinished partition into two new partitions, where now the old and recently finished process can work together. This would require a rather tedious implementation, where we also have to consider not splitting a partition too much (due to the overhead of splitting it) and implement a proper synchronization. A simpler approach would be to do a little pre-computation on some sample points in the domain to determine, how computationally intensive a partition could be. Then, the grid would be split up into unequal partitions, with compute-intensive partitions being smaller compared to less diverging partitions. This method would create some overhead for the analysis of the domain, but would probably be still much faster, especially on large image sizes and a high \lstinline|MAX_ITERS| size. In practice, a mix of both methods would be probably the way to go, leveraging the advantage of having a \emph{somewhat} correctly decomposed domain in the beginning and being, but still being able to correct it on the fly.

\section{Parallel matrix-vector multiplication and the power method}
The \emph{power method} is an iterative method which approximates the largest eigenvalue and its corresponding eigenvector of a diagonalizable matrix $A$. The method starts with an initial guess (or random vector in our case) $y$, which is then recursively iterated as
\begin{align*}
	y_{i+1} = \frac{A y_i}{||Ay_i||}
\end{align*}
Once the maximal number of iteration is reached, or $|y_{i+1}-y_{i}| \leq \text{tol}$, the algorithm will stop. In the parallel implementation the matrix-vector multiplication $Ay$ is done in parallel, dividing the processes among different rows of the matrix. This means, that every process will work on one portion of $y$, called \lstinline|y_local| and compute the corresponding row entries:
\begin{lstlisting}
for(int i_local = 0; i_local < nrows_local; i_local++) {
  y_local[i_local] = 0.;
  for(int j_global = 0; j_global < n; j_global++) {
    y_local[i_local] += A[i_local*n + j_global] * v[j_global];
  }
}
\end{lstlisting}
Then, the all the local contributions are collected into the main array \lstinline|y| using \lstinline|MPI_Allgatherv|, which can handle different buffer sizes from different processes. For that, the arrays \lstinline|recvcounts| and \lstinline|displs| need to be filled, which contain the size and starting point of \lstinline|y_local| for every process respectively:
\begin{lstlisting}
// Size of y_local for every process
for(int i = 0; i < size; i++) {
  if(i < n % size) {
    recvcounts[i] = n / size + 1;
  } else {
    recvcounts[i] = n / size;
  }
}

//Starting point of y_local for every process
displs[0] = 0;
for(int i = 1; i < size; i++) {
  displs[i] = displs[i - 1] + recvcounts[i - 1];
}

// Get all y_local contributions and store them in y    
MPI_Allgatherv(y_local, nrows_local, MPI_DOUBLE, \
y, recvcounts, displs, MPI_DOUBLE, MPI_COMM_WORLD);
\end{lstlisting}
For the strong scaling study we do two runs; one with all MPI processes on one node and a second one on multiple nodes. The single-node runs were launched with this handy script:
\begin{lstlisting}
num_procs=64
matrix=3
n=10000
iters=3000
tol=-1

for ((procs=1; procs<=num_procs; procs*=2))
do
  time mpirun -np $procs ./powermethod_rows $matrix $n $iters $tol
done
\end{lstlisting}
The results of the strong scaling runs can be seen in Figures \ref{strong_scaling_1} (1 node) and \ref{strong_scaling_2} (64 nodes).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{strong_scaling_one_node.pdf}
	\caption{Strong scaling of parallel power method on 1 node (AMD EPYC 7763) with 1-64 MPI tasks}
	\label{strong_scaling_1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{strong_scaling_64_nodes.pdf}
	\caption{Strong scaling of parallel power method on 64 nodes (AMD EPYC 7763) with 1-64 MPI tasks}
	\label{strong_scaling_2}
\end{figure}
Both runs (single and multi-node) are very similar up to 32 MPI tasks and achieve comparable speedup (although this multi-node run achieves a bit better for 16 and 32 tasks). However, with 64 tasks, the single-node run performs much worse, even being slower then on only 16 tasks, whereas the multi-node run achieves further speedup, up to 19 times faster compared to the single-process time. This can be explained with cache-locality; for the single-node run with 64 processes on 1 node, much more cores are occupied and potentially caches shared among different processes. This can lead to full caches, which results into lookups in the RAM, which significantly affects performance. The multi-node run however, doesn't experience this problem (as the caches for every process are generally empty). Even considering the much larger communication overhead between nodes, the many-nodes run on unsaturated nodes performs much better for more MPI tasks.

For the examination of weak scaling, we will perform runs with 1,2,4,8,16,32 and 64 processes and with matrices proportionally growing with a baseline of $n=1000$, i.e. ($n=$1000 $\sqrt{1}$, 1000 $\sqrt{2}$, 1000 $\sqrt{4}$ \dots). The results of weak scaling can be found in Figures \ref{weak_scaling_1} and \ref{weak_scaling_2} respectively. Figure \ref{weak_scaling_1} confirms the huge spike for 64 MPI tasks, which was already discussed in the strong scaling case. We see that the runtimes can be divided into two intervals; [1,2] and [4,16] MPI tasks. For 1 and 2 MPI tasks, the runtime is nearly identical at $\approx$3.9s, where as for 4-16 MPI tasks, we have runtimes of around 4.7s with a small spike at 4 MPI tasks. The sudden spikes could be explained, if the physical cores next to each other start to share caches, which would lead to decreasing performance due to more cache misses. 
For the multi-node run (Figure \ref{weak_scaling_2}) we see a more typical weak scaling result, with around linearly growing execution times. It's by no means a \emph{perfect} weak scaling, which also makes sense, considering the still large sequential part of the application. However, increasing the problem size by 64 times, "only" increases the runtimes by around 2 times, so the parallel implementation definitely makes sense in this application. From the result presented here, we can also confirm, that the matrix-vector multiplication must make up the biggest part of execution time, considering good speedup observed in the parallel implementation.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{weak_scaling_one_node.pdf}
	\caption{Weak scaling of parallel power method on 1 node (AMD EPYC 7763) with 1-64 MPI tasks}
	\label{weak_scaling_1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{weak_scaling_64_nodes.pdf}
	\caption{Weak scaling of parallel power method on 64 nodes (AMD EPYC 7763) with 1-64 MPI tasks}
	\label{weak_scaling_2}
\end{figure}

\end{document}
