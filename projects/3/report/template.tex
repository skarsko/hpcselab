\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\input{assignment.sty}
%Custom Code
\usepackage{listings}
\usepackage{sourcecodepro}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{amsmath}
\usepackage{float}
\usepackage{makecell}

\graphicspath{{images/}} 

\lstset{basicstyle=\footnotesize\ttfamily, language=bash, showstringspaces=false}
\lstset{frame=single}
\lstset{
	showlines=true
}


\newcommand{\br}{\vspace{8pt}\\}
\newcommand{\brr}{\vspace{8pt}}
%End Custom Code
\begin{document}


\setassignment
\setduedate{Monday 15 April 2024, 23:59 (midnight)}

\serieheader{High-Performance Computing Lab for CSE}{2024}
            {Student: Samuel Karsko}
            {Discussed with: -}{Solution for Project 3}{}
\newline


\section{Task: Implementing the linear algebra functions and the stencil
         operators}
The serial implementation of the linear algebra kernel in \lstinline|linalg.cpp| and the stencil in \lstinline|operators.cpp| is straightforward. The BLAS reductions and vector-vector operations are mostly carried out in \lstinline|for|-loops over the array size, multiplying or adding the single elements using elementary operations. The stencil kernel involves adding and multiplying the correct constants with the population concentrations $s_{ij}^k$ or $s_{ij}^{k-1}$, according to the formula
\begin{align*}
	f_{ij}^k = -(4+\alpha)s_{ij}^k + s_{i-1,j}^k + s_{i+1,j}^k + s_{i,j-1}^k + s_{i,j+1}^k + \beta s_{ij}^k (1-s_{ij}^k) + \alpha s_{ij}^{k-1}
\end{align*}
Now the application can be compiled and run sequentially. Figure \ref{fig:output_seq} shows the population density on a 512$\times$512 grid after 300 timesteps over the time-interval $[0,0.005]$ and took ~8.9 seconds to run.
\begin{figure}[H]
	\centering
	\includegraphics[width=220px]{output_seq.png}
	\caption{Solution of Fishers Equation for population dynamics on a 512$\times$512 grid after 300 timesteps in the interval $t\in[0,0.005]$. The color represents the population density at an $(x,y)$-coordinate.}
	\label{fig:output_seq}
\end{figure}

\section{Task:  Adding OpenMP to the nonlinear PDE mini-app}
We can detect if the application was compiled with OpenMP by checking, if the \lstinline|_OPENMP| macro variable has been set. If yes, we can set the number of threads to \lstinline|omp_get_max_threads()|, otherwise to 0. Conditionally, we display either the message for a sequential or OMP run:
\begin{lstlisting}
(!threads && std::cout << "version   :: C++ Serial" << std::endl) ||
(std::cout << "version   :: C++ OMP\nthreads   :: " << threads << std::endl);	
\end{lstlisting}
The linear algebra kernel can easily be parallelized using simple OMP directives. The BLAS level 1 reduction can be parallelized using \lstinline|#pragma omp parallel for reduction(+:result)|, because multiple threads will need to add to the scalar \lstinline|result|. The \lstinline|hpc_fill| routine and all other BLAS level 1 vector-vector operations can simply be parallelized with \lstinline|#pragma omp parallel for| such that threads will work on different parts of the result-vector at the same time. From first observations, a threaded version with 4 threads has a rough speedup of ~2 compared to the sequential implementation. The stencil operator can also be parallelized with OMP. I've chosen to add a \lstinline|#pragma omp parallel for private(i)| directive to the outer loop. This means, that the threads can work on different columns of the grid (while setting the row-index \lstinline|i| as private for every thread). For the inner loop I've also added \lstinline|#pragma omp simd| to add SIMD instructions and further improve the performance. The west and east boundaries can be directly parallelized with \lstinline|#pragma omp parallel|. The same can be also done for the south and north boundary, excluding the corners. The corners are being computed sequentially, however this shouldn't have any considerable performance impact, as there are "only" 4 corners. 

Producing bit-identical results with OMP would be very challenging, because floating-point arithmetic is not associative. This means, that $(a+b)+c \neq a+(b+c)$. As we use the \lstinline|reduction|-clause for some OMP operations, the single contributions from the different threads are added in a different order, e.g. \lstinline|i=0,3,1,4,2,5| compared to the sequential order \lstinline|0,1,2,3,4,5|.

We can analyze strong scaling for the application, by changing the number of OMP threads for a fixed problem size. The results are shown in Figure \ref{fig:strong_scaling}. We see, that for small grid sizes (64$\times$64 and 128$\times$128 grids), threading results in larger runtimes. This is due to the overhead created by OMP threads, when the workload is only small. For the medium grid sizes (256$\times$256 and 512$\times$512) we observe better runtimes with a few threads. The 256$\times$256 grid for example improves in runtime with up to 8 threads, but is slower again with more threads. Here once again, dividing the workload among more threads results in slower runtime due to the additional overhead. The 1024$\times$1024 grid doesn't experience this loss of improvement for up to 16 threads, but would also do so if more threads would've been used.
\begin{figure}[H]
	\centering
	\includegraphics[width=420px]{strongScaling.pdf}
	\caption{Strong Scaling Graph (Log/Log)}
	\label{fig:strong_scaling}
\end{figure}
Weak Scaling can be observed by keeping the workload per core constant. We fix the workload per core to 256$\times$256 grid points and vary the resolution from 256$\times$256 up to 2048$\times$2048. The results of weak scaling can be seen in Figure \ref{fig:weak_scaling}. We see, that the weak scaling observed is not very optimal. A larger number of threads cannot make up for larger grid sizes. The application runs for ~0.5s with 1 OMP thread and a 256$\times$256 grid, whereas it runs for nearly ~20s (40 times more) for a 2048$\times$2048 grid (which is 64 times larger). This means, that while more threads still help compared to a sequential implementation, their effect doesn't scale well for larger grid sizes. Possible reasons for this "bad" weak scaling are a still substantial sequential portion of code, memory bandwidth limitations and inter-thread communication overheads. Also, accessing and moving data from memory to the cores becomes a bottleneck for large grids, where the volume of the data is much higher.

\begin{figure}[H]
	\centering
	\includegraphics[width=420px]{weakScaling.pdf}
	\caption{Weak Scaling Graph (Log/Log)}
	\label{fig:weak_scaling}
\end{figure}
\noindent\emph{Note: All scripts for running and plotting can be found in the \lstinline|source/utils|-directory of this submission.}

\end{document}
