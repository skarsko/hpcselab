import matplotlib.pyplot as plt
import numpy as np

def read_data(filename):
    data = {}
    with open(filename, 'r') as file:
        for line in file:
            grid_size, threads, time_taken = line.strip().split(',')
            threads = int(threads)
            time_taken = float(time_taken)
            
            if grid_size not in data:
                data[grid_size] = {}
            if threads not in data[grid_size]:
                data[grid_size][threads] = []
                
            data[grid_size][threads].append(time_taken)
    
    # Calculate the median time for each configuration
    for grid_size in data:
        for threads in data[grid_size]:
            times = data[grid_size][threads]
            median_time = np.median(times)
            data[grid_size][threads] = median_time
            
    return data

def plot_strong_scaling(data):
    plt.figure(figsize=(10, 6))
    
    for grid_size, threads_data in data.items():
        threads = sorted(threads_data.keys())
        median_times = [threads_data[thread] for thread in threads]
        plt.plot(threads, median_times, marker='o', label='Grid size: {}'.format(grid_size))
    
    plt.xlabel('Number of Threads', fontsize=14)
    plt.ylabel('Median Time Taken (seconds)', fontsize=14)
    plt.title('Strong Scaling Analysis', fontsize=16)
    plt.legend()
    plt.grid(True)
    plt.xscale('log')
    plt.yscale('log')
    plt.xticks(threads, threads)  # Assuming the number of threads is consistent across grid sizes
    plt.tight_layout()
    plt.savefig('strongScaling.pdf')
    plt.show()

# Replace 'extracted_data.txt' with the path to your data file if different
data = read_data('extracted_data.txt')
plot_strong_scaling(data)
