def extract_data_from_file(input_filename, output_filename):
    with open(input_filename, 'r') as infile:
        lines = infile.readlines()

    data_to_write = []

    # Variables to hold the current values being extracted
    threads, grid_size, time_taken = None, None, None

    for line in lines:
        if 'threads' in line:
            # Extract the number of threads
            threads = line.split('::')[1].strip()
        elif 'mesh' in line:
            # Extract the grid size
            grid_size = line.split('*')[0].split('::')[1].strip()
        elif 'simulation took' in line:
            # Extract the time taken
            time_taken = line.split(' ')[2].strip()
            
            # Once all required data for a set is extracted, append to the data list
            data_to_write.append("{},{},{}".format(grid_size, threads, time_taken))
    
    # Write the extracted data to the output file
    with open(output_filename, 'w') as outfile:
        for data in data_to_write:
            outfile.write(data + '\n')

# Define the input and output file names
#input_file = "strongScaling.txt"
input_file = "weakScaling.txt"
output_file = "extracted_data.txt"

# Call the function to extract data and write to the output file
extract_data_from_file(input_file, output_file)

# Let's print a message to confirm the operation is completed
print("Data extraction completed and written to 'extracted_data.txt'.")
