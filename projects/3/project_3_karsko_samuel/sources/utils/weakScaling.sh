#!/bin/bash
#SBATCH --job-name=weakScaling
#SBATCH --output=output.txt
#SBATCH --error=error.txt
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=128
#SBATCH --mem-per-cpu=1G
#SBATCH --constraint=EPYC_7763
ml gcc
cd .. && make clean && make && cd utils 
cp ../main .
dt=100
t_end=0.005
repetitions=10

NCPU=(1 4 16 64)
n=(256 512 1024 2048)

rm -f weakScaling.txt
touch weakScaling.txt

for i in {0..3}; do
    for ((j=0; j<repetitions; j++)); do
        echo "Running weak scaling test with n=${n[i]} and NCPU=${NCPU[i]}:"
        export OMP_NUM_THREADS=${NCPU[i]}          
        ./main ${n[i]} $dt $t_end >> weakScaling.txt  
    done      
done

echo "Weak scaling test completed"