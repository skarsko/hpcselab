import matplotlib.pyplot as plt
import numpy as np

def read_data(filename):
    data = {}
    with open(filename, 'r') as file:
        for line in file:
            grid_size, threads, time_taken = line.strip().split(',')
            threads = int(threads)
            time_taken = float(time_taken)
            
            if threads not in data:
                data[threads] = []
                
            data[threads].append(time_taken)
    
    # Calculate the median time for each thread configuration
    for threads in data:
        times = data[threads]
        median_time = np.median(times)
        data[threads] = median_time
            
    return data

def plot_weak_scaling(data):
    plt.figure(figsize=(10, 6))
    
    threads = sorted(data.keys())
    median_times = [data[thread] for thread in threads]
    plt.plot(threads, median_times, marker='o', label='Weak Scaling, 256*256 grid per OMP thread')
    
    plt.xlabel('Number of Threads', fontsize=14)
    plt.ylabel('Median Time Taken (seconds)', fontsize=14)
    plt.title('Weak Scaling Analysis', fontsize=16)
    plt.legend()
    plt.grid(True)
    plt.xscale('log')
    plt.yscale('log')
    plt.xticks(threads, threads)
    plt.tight_layout()
    plt.savefig('weakScaling.pdf')
    plt.show()

data = read_data('extracted_data.txt')
plot_weak_scaling(data)
