#!/bin/bash
#SBATCH --job-name=strongScaling
#SBATCH --output=output.txt
#SBATCH --error=error.txt
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mem-per-cpu=1G
#SBATCH --constraint=EPYC_7763
ml gcc
cd .. && make clean && make && cd utils 
cp ../main .
dt=100
t_end=0.005
repetitions=5

rm -f strongScaling.txt
touch strongScaling.txt
for n in 64 128 256 512 1024; do
    for NCPU in 1 2 4 8 16; do  
        for ((i=0; i<repetitions; i++)); do
            echo "Running strong scaling test with n=$n and NCPU=$NCPU:"
            export OMP_NUM_THREADS=$NCPU          
            ./main $n $dt $t_end >> strongScaling.txt  
        done      
    done
done
echo "Strong scaling test completed"