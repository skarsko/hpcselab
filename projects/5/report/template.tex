\documentclass[unicode,11pt,a4paper,oneside,numbers=endperiod,openany]{scrartcl}

\input{assignment.sty}


%Custom Code
\usepackage{listings}
\usepackage{sourcecodepro}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{amsmath}
\usepackage{float}
\usepackage{makecell}

\graphicspath{{images/}} 

\lstset{basicstyle=\footnotesize\ttfamily, language=bash, showstringspaces=false}
\lstset{frame=single}
\lstset{
	showlines=true
}


\newcommand{\br}{\vspace{8pt}\\}
\newcommand{\brr}{\vspace{8pt}}
%End Custom Code


\begin{document}


\setassignment
\setduedate{Monday 13 May 2024, 23:59 (midnight).}

\serieheader{High-Performance Computing Lab for CSE}{2024}
            {Student: Samuel Karsko}
            {Discussed with: -}{Solution for Project 5}{}
\newline


\section{Parallel Space Solution of a nonlinear PDE using MPI}
In this task, we parallelize the Fisher's equation solver from project 3 using MPI. 
\subsection{Initialize/finalize MPI and welcome message}
After initializing MPI at the beginning of the \lstinline|main|-function, we want the user to know, that they're using the MPI implementation of the PDE solver and therefore extend the welcome message by also printing the number of processes, which are stored in the variable \lstinline|size|. We can ensure, that the message is printed only once by printing it only for the rank 0 process. Also, we'll add a \lstinline|MPI_Finalize()| at the end of the \lstinline|main|-function, such that we now work in a proper MPI environment and can start with the proper parallelization.
\subsection{Domain decomposition}
We'll now focus on the \lstinline|data.cpp| file, where the domains and subdomains are handled. We'll use the inbuilt Cartesian topology in MPI to split up the domain into smaller subdomains, for each process to work on.	Using \lstinline|MPI_Dims_create| we can determine an optimal number of sub-domains in $x$ and $y$ direction and save that information into \lstinline|ndomy| and \lstinline|ndomx|. Using \lstinline|MPI_Cart_create| we can create a non-periodic Cartesian 2D topology, from where can retrieve the coordinates for every rank using \lstinline|MPI_Cart_coords|. Finally, using \lstinline|MPI_Cart_shift| we can determine the neighbours of every process, which finalizes our domain:
\begin{lstlisting}
MPI_Dims_create(mpi_size, 2, dims);
ndomy = dims[0];
ndomx = dims[1];

MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &comm_cart);

MPI_Cart_coords(comm_cart, mpi_rank, 2, coords);
domy = coords[0] + 1;
domx = coords[1] + 1;

MPI_Cart_shift(comm_cart, 0, 1, &neighbour_south, &neighbour_north);
MPI_Cart_shift(comm_cart, 1, 1, &neighbour_west, &neighbour_east);
\end{lstlisting}
In my opinion 2D subdomains on a Cartesian grid is the most straight-forward, but also most efficient approach of domain decomposition in this case. MPI handles the number of processes in $x$ and $y$ direction automatically, which ensures optimal distribution of the processes. Using this approach, we also help with load balancing, as \lstinline|MPI_Dims_create| distributes the grid points as evenly as possible among the processes. Also, using 2D subgrids, the communication is limited to neighbours-only, which will mostly be physically close processes.

\subsection{Linear algebra kernels}
Some BLAS operations are suitable for MPI-Parallelization; in the file \lstinline|linalg.cpp| we find two main operations which are well suited:
\begin{itemize}
	\item \lstinline|hpc_dot|: Computes the dot-product of two vectors.
	\item \lstinline|hpc_norm2|: Computes the squared norm of a vector.
\end{itemize}
These operations are BLAS level 1 reduction operations which are most suitable for MPI parallelization, as they are not-data-dependent and only need one reduction operation for the communication. Both can be implemented with summing local contributions to one global value using \lstinline|MPI_Allreduce|.
\subsection{The diffusion stencil: Ghost cells exchange}
For implementing the ghost cells exchange in the diffusion stencil, we proceed similar to exercise 3. We can use the provided buffers and boundaries in all directions and send/receive the neighbouring data to/from them. We check if a neighbour exists, by checking if the provided rank (north/west/east/south) is equal or grater then 0, a rank of -1 would indicate that there's no neighbour, which will be at the non-periodic boundaries. We'll use non-blocking operations (\lstinline|MPI_Isend| and \lstinline|MPI_Irecv|) followed by an \lstinline|MPI_Waitall| after the interior grid points have been computed. At this time, most tasks should be already done with the communication and the barrier will likely not be triggered very often. The final implementation looks as follows:
\begin{lstlisting}
//Copy populalation at the boundaries into the buffer for sending
for (int i = 0; i < nx; i++) {
  buffN[i] = s_new(i, ny-1);
  buffS[i] = s_new(i, 0);
}
for (int j = 0; j < ny; j++) {
  buffW[j] = s_new(0, j);
  buffE[j] = s_new(nx - 1, j);
}

//Non-blocking ghost cell exchange
if (north >= 0) {
  MPI_Isend(buffN.data(), nx, MPI_DOUBLE, north, 0, domain.comm_cart, &reqs[count++]);
  MPI_Irecv(bndN.data(), nx, MPI_DOUBLE, north, 0, domain.comm_cart, &reqs[count++]);
}
if (south >= 0) {
  MPI_Isend(buffS.data(), nx, MPI_DOUBLE, south, 0, domain.comm_cart, &reqs[count++]);
  MPI_Irecv(bndS.data(), nx, MPI_DOUBLE, south, 0, domain.comm_cart, &reqs[count++]);
}
if (east >= 0) {
  MPI_Isend(buffE.data(), ny, MPI_DOUBLE, east, 0, domain.comm_cart, &reqs[count++]);
  MPI_Irecv(bndE.data(), ny, MPI_DOUBLE, east, 0, domain.comm_cart, &reqs[count++]);
}
if (west >= 0) {
  MPI_Isend(buffW.data(), ny, MPI_DOUBLE, west, 0, domain.comm_cart, &reqs[count++]);
  MPI_Irecv(bndW.data(), ny, MPI_DOUBLE, west, 0, domain.comm_cart, &reqs[count++]);
}

//Compute interior gridpoints
// ...

MPI_Waitall(req_count, reqs, stats);   
\end{lstlisting}
\subsection{Implement parallel I/O}
Finally we'll implement parallel MPI I/O for writing the output data.
For that we'll start with opening/creating the output file with the passed name in write-only mode using \lstinline|MPI_File_open|:
\begin{lstlisting}
MPI_File_open(domain.comm_cart, fname.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
\end{lstlisting}
For parallel I/O we first create a subarray type, which will include the portion of the array that each process writes to:
\begin{lstlisting}
MPI_Datatype subarray;
MPI_Type_create_subarray(2, sizes, subssize, starts, MPI_ORDER_FORTRAN, \
  MPI_DOUBLE, &subarray);
MPI_Type_commit(&subarray);    
\end{lstlisting}
Before writing to the file, we'll use \lstinline|MPI_File_set_view| which sets the file view for each process. This means, it configures for every process, where it should write and it also configures the offsets automatically. Finally, we can start the write-operations with
\begin{lstlisting}
MPI_File_write_all(fh, u.data(), domain.nx * domain.ny, MPI_DOUBLE, &st);
\end{lstlisting} 
Finally, we'll clean up by freeing the subarray and file handle.
\subsection{Strong and weak scaling}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.49\textwidth]{strong_scaling_high.pdf}
	\includegraphics[width=0.49\textwidth]{weak_scaling_high.pdf}
	\label{weak_strong_scaling}
	\caption{Weak and strong scaling of the mini PDE solver for the Fisher's equation parallelized with MPI. The resolutions range from 64$\times$64 to $2048\times2048$ with varying MPI tasks from 1 to 64. The tests were ran on the Euler-Cluster on an AMD EPYC 7763 CPU and the data was recorded as an average of 5 runs for each configuration.}
\end{figure}
Strong and weak scaling analysis was performed by running a variety of configurations (grid size and number of MPI tasks) on an AMD EPYC 7763 CPU on the Euler cluster. The visualized data can be seen in Figure \ref{weak_strong_scaling}. We see from strong scaling, that for a large enough resolution, MPI improves the performance quite considerably, but the improvement is generally a bit lower compared to the OpenMP implementation from exercise 3. Larger grid sizes (256$\times$256 or higher) are most suited for the MPI implementation, whereas for the lowest tested resolution (64$\times$64) more MPI tasks worsen the performance. When looking at the weak scaling graph, the picture dims even more; the application doesn't really scale well and more MPI tasks cannot compensate for bigger grid sizes. Sequential parts of the application and the higher communication overhead with for example the ghost cell exchanges thwart optimal scaling. However, the communication between ranks which comes with MPI is MPI's biggest overhead, but also it's biggest advantage compared to shared-memory parallelization like OpenMP. For a maximum number of 64 cores (1 CPU) we don't see the advantages of MPI yet, but one could scale out this application to thousands of nodes for gigantic problem sizes, whereas OpenMP is only very limited, as it requires shared memory and isn't suited for multi-nodal applications. Therefore the comparable, or slightly lower¨ performance of this MPI implementation compared to the OpenMP implementation in exercise 3 isn't really an issue, as the use case is completely different. If one would like to achieve optimal performance in a real-life setting, a hybrid MPI-OpenMP implementation would be the most efficient way, where every MPI rank has it's own portion of the domain, only communicating if necessary (ghost-cell exchange and I/O) and where OpenMP shared memory parallelization takes over more fine-grained parallelism, as for example the BLAS routines.
\section{Python for High-Performance Computing [in total 40 points]}

\subsection{Sum of ranks: MPI collectives [5 Points]}

\subsection{Ghost cell exchange between neighboring processes [5 Points]}

\subsection{A self-scheduling example: Parallel Mandelbrot [30 Points]}


\end{document}
