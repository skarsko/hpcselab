#!/bin/bash

resolutions=(64 128 256 512 1024)
processes=(1 2 4 8 16 32 64)
repetitions=5

# Loop over resolutions
for n in "${resolutions[@]}"
do
    # Loop over processes
    for np in "${processes[@]}"
    do
        total_time=0
        # Perform the simulation multiple times to get an average
        for (( i=1; i<=$repetitions; i++ ))
        do
            # Capture the time to solution
            time_to_solution=$(mpirun -np $np main $n 100 0.005 | grep -oP 'simulation took \K[\d.]+')
            total_time=$(echo "$total_time + $time_to_solution" | bc)
        done

        # Calculate average time to solution
        average_time=$(echo "$total_time / $repetitions" | bc -l)

        # Print the result
        printf "Resolution: %d x %d, Processes: %d, Average Time to Solution: %.3f\n" "$n" "$n" "$np" "$average_time"
    done
done
