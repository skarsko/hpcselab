import matplotlib.pyplot as plt

plt.style.use('seaborn-v0_8-dark')

resolutions = [64, 128, 256, 512, 1024]
ncpus = [1, 4, 16, 64]

# Define the time to solution for each configuration
time_to_solution = [
    # MPI-Tasks = 1, Sizes = 64, 128, 256, 512, 1024
    [0.027, 0.148, 1.005, 7.956, 92.389],

    # MPI-Tasks = 4, Sizes = 64, 128, 256, 512, 1024
    [0.022, 0.078, 0.425, 2.640, 16.683],
    
    # MPI-Tasks = 16, Sizes = 64, 128, 256, 512, 1024
    [0.027, 0.041, 0.147, 0.904, 5.307],
    
    # MPI-Tasks = 64, Sizes = 64, 128, 256, 512, 1024, 2048
    [0.428, 0.042, 0.083, 0.382, 1.672, 19.4301]
]

plt.figure(figsize=(6, 8))

# Plot the data
#Base resolution 64
plt.plot(ncpus, [time_to_solution[0][0], time_to_solution[1][1], time_to_solution[2][2], time_to_solution[3][3]], label="64^2 points / task")
#Base resolution 128
plt.plot(ncpus, [time_to_solution[0][1], time_to_solution[1][2], time_to_solution[2][3], time_to_solution[3][4]], label="128^2 points / task")
#Base resolution 256
plt.plot(ncpus, [time_to_solution[0][2], time_to_solution[1][3], time_to_solution[2][4], time_to_solution[3][5]], label="256^2 points / task")

plt.xscale("log")
plt.yscale("log")

plt.xticks(ncpus, ncpus)

plt.xlabel("Number of MPI tasks", fontsize=12)
plt.ylabel("Time to Solution (sec.)", fontsize=12)
plt.title("Weak Scaling of MPI Fisher's Equation Solver", fontsize=14)
plt.legend()

plt.savefig("weak_scaling.pdf")