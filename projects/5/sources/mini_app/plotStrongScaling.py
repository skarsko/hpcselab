import matplotlib.pyplot as plt

plt.style.use('seaborn-v0_8-dark')

resolutions = [64, 128, 256, 512, 1024]
ncpus = [1, 2, 4, 8, 16]

times_64 = [0.027, 0.016, 0.022, 0.024, 0.027]  
times_128 = [0.148, 0.077, 0.078, 0.049, 0.041]  
times_256 = [1.005, 0.520, 0.425, 0.258, 0.147] 
times_512 = [7.956, 3.818, 2.640, 1.586, 0.904]  
times_1024 = [92.389, 60.126, 16.683, 8.483, 5.307]

plt.figure(figsize=(6, 8))

plt.plot(ncpus, times_64, label="64")
plt.plot(ncpus, times_128, label="128")
plt.plot(ncpus, times_256, label="256")
plt.plot(ncpus, times_512, label="512")
plt.plot(ncpus, times_1024, label="1024")

plt.yscale("log")



plt.xlabel("Number of MPI tasks", fontsize=12)
plt.ylabel("Time to Solution (sec.)", fontsize=12)
plt.title("Strong Scaling of MPI Fisher's Equation Solver", fontsize=14)
plt.legend()
plt.savefig("strong_scaling.pdf")
